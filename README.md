# Hanabot-Rust
Experimental rewrite of [Hanabot](https://gitlab.com/hana.flower/hanabot) in Rust using Serenity. Still in early development.

**DO NOT USE THIS IN PRODUCTION YET OTHERWISE YOUR DISCORD SERVER WILL IMPLODE.**

# Installation
`<1>` Install CouchDB services in your system. Refer to [CouchDB](https://docs.couchdb.org/en/stable/install/index.html) documentation to find out how to do this in your system.

`<2>` Install Rust via the `rustup` toolchain. Refer to the [Rust](https://doc.rust-lang.org/book/ch01-01-installation.html) Documentation to find out how to do this in your system.

`<3>` Open the terminal and run `cargo run` to generate the `config.json` file. `(Ignore any compiler warnings.)`

`<4>` Fill up the `config.json` file with necessary information.

`<5>` Run `cargo build --release` in your terminal.

`<6>` Great! Your `Hanabot-Rust` binary is ready. The file can be found in `/target/release/hanabot_rust`.

`<7>` Navigate to `target/release/` and run the `hanabot_rust` binary file.

`<8>` Enjoy! ^-^

This README will be finished later
=======
Hanabot is a utility bot designed for NSFW servers. That's about all I can say right now.

Features:

* Claiming is in the works
* Everything else is also in the works lol