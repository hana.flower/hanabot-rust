use std::collections::HashMap;
use poise::serenity_prelude::{self as serenity, ActionRowComponent, GuildId, Member, User};
use poise::serenity_prelude::Interaction;
use serenity::all::{EventHandler, Message};
use serenity::async_trait;
use serenity::prelude::Context;

/// This is the event handler for the bot.
pub struct Handler;  

#[async_trait]
impl EventHandler for Handler {
    /// The member leave event hook.
    async fn guild_member_removal(&self, ctx: Context, guild_id: GuildId, user: User, member: Option<Member>) {

    }

    /// The message received event hook.
    async fn message(&self, ctx: Context, msg: Message) {
        super::custom_hooks::handle_message_hooks(ctx, msg).await; // This is the custom message hook.
    }

    /// The interaction create event hook.
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::Component(inter) = interaction {
            let custom_id: &String = &inter.data.custom_id;

            // Getting the type of interaction, e.g. claim contract, settings, etc
            let args: Vec<&str> = custom_id.split("::").collect::<Vec<&str>>();
            let interaction_type: &str = args[0].trim();

            match interaction_type {
                "CLAIM_CONTRACT_INTERACTION" => { crate::commands::claim_cmds::claim::contract_interaction(ctx.clone(), inter.clone()).await }
                "SETTINGS" => { crate::commands::settings_cmds::settings_menu(ctx.clone(), inter.clone(), args).await }
                "RULES" => { crate::commands::rules_cmds::rules_accept(ctx.clone(), inter.clone(), args).await }
                _ => { return }
            }
        }
        else if let Interaction::Modal(inter) = interaction {
            let custom_id: &String = &inter.data.custom_id;

            // Getting the type of interaction, e.g. claim contract, settings, etc
            let args: Vec<&str> = custom_id.split("::").collect::<Vec<&str>>();
            let interaction_type: &str = args[0].trim();

            // All the options as Strings.
            let mut option_values: &mut HashMap<String, String> = &mut HashMap::new();
            for action_row in &inter.data.components {
                // InputText, which is expected, always fills up the entire action row.
                match &action_row.components[0] {
                    ActionRowComponent::InputText(component) => {
                        option_values.insert(component.custom_id.clone(), component.value.clone().unwrap());
                    }
                    _ => { panic!("The action row did not contain an InputText; this should never happen for a modal.") }
                }
            }

            match interaction_type {
                "SETTINGS" => { crate::commands::settings_cmds::settings_menu_modal(ctx.clone(), inter.clone(), args, option_values).await }
                "MEMBER_AGE_CHECK" => { crate::commands::member_age_check::member_age_check_answer(ctx.clone(), inter.clone(), option_values).await }
                _ => { return }
            }
        }
    }
}
