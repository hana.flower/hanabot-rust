#![warn(clippy::pedantic)]

use std::sync::Arc;
use log::error;
use poise::{serenity_prelude as serenity, Framework};
use serenity::ShardManager;
use serenity::all::{ActivityData, GatewayIntents, OnlineStatus};
use serenity::prelude::TypeMapKey;
use serenity::Client;
use utils::config_handler::Config;
use utils::couch_db::CouchDB;
mod commands;
mod custom_hooks;
mod event_handler;
mod utils;
mod guild_settings;

#[tokio::main]
async fn main() {
    // Load the config file
    let config: Config = Config::load_config("./config.json");
    let config_copy: Config = config.clone();

    // Get the token from the config file
    let token: &str = &config.token;

    // Get the presence from the config file
    let presence: Presence = Presence::new(
        &config.presence.online_status,
        &config.presence.activity_type,
        &config.presence.activity_text,
    );

    // List gateway intents.
    let intents: GatewayIntents = GatewayIntents::GUILDS
        | GatewayIntents::GUILD_MEMBERS
        | GatewayIntents::GUILD_MODERATION
        | GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES;

    // Create the CouchDB Client
    let couch_db: CouchDB = CouchDB::new(&config.couch_db_creds)
        .unwrap_or_else(|error| panic!("Couldn't connect to database: {error}"));
    let couch_db_copy: CouchDB = couch_db.clone();

    // Create the poise framework:
    let framework: Framework<Data, _> = Framework::builder()
        .options(poise::FrameworkOptions {
            commands: vec![commands::claim_cmds::claiming(), commands::ping_cmd::ping(), commands::settings_cmds::settings()], // Register Slash Commands.
            prefix_options: poise::PrefixFrameworkOptions {
                prefix: Some("~".to_owned()),
                additional_prefixes: vec![
                    poise::Prefix::Literal("hanabot "),
                    poise::Prefix::Literal("hana-bot "),
                ], // Register Prefix for Prefix Commands.
                ..Default::default()
            },
            ..Default::default()
        })
        .setup(|ctx, _ready, framework| {
            // there is a move block here, this will Yoink your data even if you pass reference inside it! Careful!
            Box::pin(async move {
                poise::builtins::register_globally(ctx, &framework.options().commands).await?;
                Ok(Data {
                    _config: config_copy,
                    couch_db: couch_db_copy,
                })
            })
        })
        .build();

    // Create the bot client
    let mut client: Client = Client::builder(token, intents)
        .intents(intents)
        .framework(framework)
        .event_handler(event_handler::Handler)
        .status(presence.online_status)
        .activity(presence.activity)
        .await
        .expect("Couldn't build client.");

    client.data.write().await.insert::<CtxConfig>(config);
    client.data.write().await.insert::<CtxCouchDB>(couch_db);
    let shard_manager: Arc<ShardManager> = client.shard_manager.clone();

    // For cleanup operations during a bot stop due to ctrl_c.
    ctrlc::set_handler(move || {
        println!("Running cleanups!");
        tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(async { custom_hooks::on_ctrl_c::on_ctrl_c(shard_manager.clone()).await });
    })
    .expect("Couldn't run cleanup operations!");

    // Starting the bot!!
    let start_message: &str = r"
█████████████████████████████████████████████████████████
█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█
█░░█░█░█▀█░█▀█░█▀█░█▀▄░█▀█░▀█▀░░░░░█▀▄░█░█░█▀▀░▀█▀░█░█░░█
█░░█▀█░█▀█░█░█░█▀█░█▀▄░█░█░░█░░▄▄▄░█▀▄░█░█░▀▀█░░█░░░█░░░█
█░░▀░▀░▀░▀░▀░▀░▀░▀░▀▀░░▀▀▀░░▀░░░░░░▀░▀░▀▀▀░▀▀▀░░▀░░░▀░░░█
█░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█
█████████████████████████████████████████████████████████

VERSION: 0.2.0 - Prelude
AUTHORS: Ray & Miss Hana
";
    println!("{start_message}");
    client
        .start()
        .await
        .expect("Bot couldn't start! Please check your token in config.json");
}

struct Data {
    _config: Config,
    couch_db: CouchDB,
} // User data, which is stored and accessible in all command invocations

struct CtxConfig;
struct CtxCouchDB;
impl TypeMapKey for CtxConfig {
    type Value = Config;
} // TypeMapKey is a trait that Serenity uses to store data in the client...
impl TypeMapKey for CtxCouchDB {
    type Value = CouchDB;
}

struct Presence {
    pub online_status: OnlineStatus,
    pub activity: ActivityData,
}

impl Presence {
    pub fn new(online_status: &str, activity_type: &str, activity: &str) -> Self {
        Presence {
            online_status: match online_status {
                "ONLINE" => OnlineStatus::Online,
                "IDLE" => OnlineStatus::Idle,
                "DO-NOT-DISTURB" => OnlineStatus::DoNotDisturb,
                "INVISIBLE" => OnlineStatus::Invisible,
                _ => {
                    error!("invalid entry on online-status. please check config.json!\nSetting to default..");
                    OnlineStatus::default()
                }
            },
            activity: match activity_type {
                "PLAYING" => ActivityData::playing(activity),
                "LISTENING" => ActivityData::listening(activity),
                "WATCHING" => ActivityData::watching(activity),
                "COMPETING" => ActivityData::competing(activity),
                _ => {
                    println!("invalid entry on activity-type. please check config.json!\nSetting to default..");
                    ActivityData::listening("Serenity~")
                }
            },
        }
    }
}
