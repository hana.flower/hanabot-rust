pub mod funny_message_response;
pub mod on_ctrl_c;

use poise::serenity_prelude as serenity;
use poise::serenity_prelude::{Mention, Mentionable};
use serenity::all::{Message};
use serenity::prelude::{Context};

// This is the custom message hook!
pub async fn handle_message_hooks(ctx: Context, message: Message) {
    let bot_mention: Mention = ctx.cache.current_user().mention();

    // Handle the "good bot" message.
    if message.content.contains(bot_mention.to_string().as_str()) && message.content.contains("good bot") {
        funny_message_response::handle_good_bot(message, ctx).await;
    }
}