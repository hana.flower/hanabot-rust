use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;
use indicatif::ProgressBar;
use poise::serenity_prelude::{ActivityData, OnlineStatus, ShardId, ShardManager, ShardMessenger, ShardRunnerInfo};
use tokio::sync::MutexGuard;

pub async fn on_ctrl_c(shard_manager: Arc<ShardManager>) {
    let shard_runners: MutexGuard<HashMap<ShardId, ShardRunnerInfo>> = shard_manager.runners.lock().await;
    let mut shards:Vec<&ShardMessenger> = vec![];

    // Iterate through all necessary shards!
    for (id, runner) in shard_runners.iter() {
        println!("Cleaning shard: {id}");
        let shard = &runner.runner_tx;
        shard.set_presence(Some(ActivityData::custom("Shutting Down~")), OnlineStatus::DoNotDisturb);
        shards.push(shard);
    }

    // Stopping the bot!
    let count_down: u64 = 5;
    println!("Bot Exiting safely in {count_down} seconds! Please wait~");
    let progress_bar: ProgressBar = ProgressBar::new(count_down);
    for _ in 0..count_down {
        progress_bar.inc(1);
        tokio::time::sleep(Duration::from_secs(1)).await;
    }
    for shard in shards {
        shard.shutdown_clean();
    }
    println!("Bot Exited Safely!");
    std::process::exit(1);
}
