use poise::serenity_prelude as serenity;
use poise::serenity_prelude::{EmojiId, ReactionType};
use serenity::all::Message;
use serenity::prelude::Context;

use crate::CtxConfig;
use crate::utils::config_handler;
use crate::utils::config_handler::Config;

// This is the custom message logic for the "good bot" message.
pub async fn handle_good_bot(message: Message, ctx: Context) {
    let config: Config = ctx.data.read().await.get::<CtxConfig>().expect("Can't get config...").clone();
    let replay_message: &str = &config.good_bot.goodbot_message;
    message.reply(&ctx.http, replay_message).await.expect("Couldn't send message!");

    let reply_reaction = &config.good_bot.goodbot_reaction;
    match reply_reaction {
        config_handler::ReactionType::UNICODE(reaction) => {
            message.react(&ctx.http, ReactionType::Unicode(reaction.clone())).await.expect("Couldn't react!");
        }
        config_handler::ReactionType::CUSTOM(reaction) => {
            let r: ReactionType = ReactionType::Custom {
                animated: reaction.animated,
                id: EmojiId::from(reaction.id),
                name: Some(reaction.name.clone())
            };
            message.react(&ctx.http, r).await.expect("Couldn't react!");
        }
    }
}
