use std::fs;
use std::io::Result;

use serde::de::DeserializeOwned;
use serde::Serialize;

///`write_json(path: &str, data: T) -> ()`
///writes a json doc into specified .json file.
pub fn write_json<T: Serialize>(path: &str, data: &T) {
    let content: Vec<u8> = serde_json::to_vec_pretty(data).expect("Couldn't serialize data.");
    fs::write(path, content).expect("Couldn't write file...");
}

///`load_json(path: &str) -> Result<T>`
///loads a specified .json file.
pub fn load_json<T: DeserializeOwned>(path: &str) -> Result<T> {
    let content: Vec<u8> = fs::read(path)?;
    let value: T = serde_json::from_slice(&content)?;
    Ok(value)
}
