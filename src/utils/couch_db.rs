use couch_rs::Client;
use couch_rs::database::Database;
use couch_rs::document::TypedCouchDocument;
use couch_rs::error::{CouchError, CouchResult};
use couch_rs::types::document::DocumentCreatedDetails;
use couch_rs::types::find::FindQuery;
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

///Wrapper for the ``couch_rs`` client supporting more specialized methods.
#[derive(Clone)]
pub struct CouchDB {
    client: Client
}

impl CouchDB {
    // Create a new client

    pub fn new(creds: &CouchDBCredentials) -> Result<Self, CouchError> {
        let couchdb: CouchDB = CouchDB { client: Client::new(creds.host.as_str(), creds.username.as_str(), creds.password.as_str())? };
        Ok(couchdb)
    }

    // Find a document using a mango query

    pub async fn find(&self, data_base: DataBase, query: Map<String, Value>) -> CouchResult<Vec<Value>> {
        let query: Value = Value::Object(query);
        let find_query: FindQuery = FindQuery::new(query);
        let doc: Vec<Value> = self.client.db(data_base.db_name().as_str()).await?.find(&find_query).await?.get_data().clone();
        Ok(doc)
    }

    // Create or update a particular document

    pub async fn create_or_update<T: Serialize + TypedCouchDocument>(&self, data_base: DataBase, doc: &mut T) -> CouchResult<DocumentCreatedDetails> {
        Ok(match self.client.db(data_base.db_name().as_str()).await?.upsert(doc).await {
            Ok(this) => { this }
            Err(_) => {
                self.client.db(data_base.db_name().as_str()).await?.create(doc).await?
            }
        })
    }

    // Delete a document from a database by CouchDB ID

    pub async fn delete_doc(&self, id: &str, db_name: DataBase) -> CouchResult<()> {
        let db: Database = self.client.db(db_name.db_name().as_str()).await?;
        if let Ok(doc) = db.get::<Value>(id).await {
            db.remove(&doc).await;
        }
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CouchDBCredentials {
    pub host: String,
    pub username: String,
    pub password: String
}

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
pub enum DataBase {
    CLAIMS,
    CLAIM_WAITLIST,
    DELETE_QUEUE,
    GUILDS,
    ONBOARDING,
    MEMBERS,
    USERS
}

impl DataBase {
    fn db_name(&self) -> String {
        match self {
            DataBase::CLAIMS => "claims".to_string(),
            DataBase::CLAIM_WAITLIST => "claim-waitlist".to_string(),
            DataBase::DELETE_QUEUE => "delete-queue".to_string(),
            DataBase::GUILDS => "guilds".to_string(),
            DataBase::ONBOARDING => "onboarding".to_string(),
            DataBase::MEMBERS => "members".to_string(),
            DataBase::USERS => "users".to_string()
        }
    }
}
