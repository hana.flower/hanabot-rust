use crate::utils::couch_db::CouchDBCredentials;
use crate::utils::json_handler;
use serde::{Deserialize, Serialize};

///The default struct for the config.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Config {
    pub token: String,
    pub couch_db_creds: CouchDBCredentials,
    pub presence: Presence,
    pub good_bot: Goodbot,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Presence {
    pub online_status: String,
    pub activity_type: String,
    pub activity_text: String,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Goodbot {
    pub goodbot_message: String,
    pub goodbot_reaction: ReactionType,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CustomReaction {
    pub animated: bool,
    pub id: u64,
    pub name: String,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
#[allow(clippy::upper_case_acronyms)]
pub enum ReactionType {
    UNICODE(String),
    CUSTOM(CustomReaction),
}

impl Config {
    pub fn load_config(path: &str) -> Self {
        let default_config: Config = Config {
            token: String::new(),
            couch_db_creds: CouchDBCredentials {
                host: "http://127.0.0.1:5984/".to_string(),
                username: String::new(),
                password: String::new(),
            },
            presence: Presence {
                online_status: "IDLE".to_string(),
                activity_type: "LISTENING".to_string(),
                activity_text: "to Serenity~".to_string(),
            },
            good_bot: Goodbot {
                goodbot_message: ":3 <3".to_string(),
                goodbot_reaction: ReactionType::CUSTOM(CustomReaction {
                    animated: true,
                    id: 1_211_384_352_223_797_279,
                    name: "test".to_string(),
                }),
            },
        };

        let config: Config = json_handler::load_json(path).unwrap_or_else(|e| {
            println!("{e}");
            json_handler::write_json(path, &default_config);
            default_config
        });
        config
    }
}
