/// Converts an integer to bitwise flags.
pub fn integer_to_bitwise_flags(int: u32) -> Vec<u32> {
    let bin_int = String::from(format!("{int:b}"));
    let mut flags: Vec<u32> = Vec::new();

    for (i, c) in bin_int.chars().rev().enumerate() {
        if c == '1' {
            let flag_value = 2u32.pow(i as u32);
            flags.push(flag_value);
        }
    }

    return flags;
}
