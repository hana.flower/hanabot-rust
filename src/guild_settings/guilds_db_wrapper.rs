use couch_rs::{types::document::DocumentCreatedDetails, CouchDocument};
use couch_rs::document::TypedCouchDocument;
use couch_rs::error::CouchError;
use poise::serenity_prelude::{ChannelId, GuildId, MessageId};
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use crate::utils;
use crate::utils::couch_db::{CouchDB, DataBase};

/// Hanabot features that the guild has enabled. These are bitwise values.
#[derive(Debug, PartialEq)]
pub enum FeaturesFlags {
    // 1 is reserved currently
    Claiming = 2,
    AccountAgeCheck = 4,
    Welcoming = 8,
    BanList = 16,
    MemberAgeCheck = 32,
    Onboarding = 64,
    CrossVerify = 128,
}

/// Hanabot-specific information for a guild.
#[derive(Serialize, Deserialize, Clone, Debug, CouchDocument, Default)]
pub struct GuildEntry {
    pub _id: String, // Guild ID
    pub _rev: String, // CouchDB rev
    pub important_channel_ids: ImportantChannels,
    pub important_role_ids: ImportantRoles,
    pub messages: Messages,
    pub features: usize, // Bitwise flags
    pub min_account_age: usize, // In hours, max 99999,
    pub reinvite_url: String,
    pub claim_limit: u8, // Max 25
    pub rules_buttons_swapped: bool,
    pub rules_message_location: Vec<String>, // [channel ID, message ID]
    pub cross_verify_guilds: Vec<String> // Vector of Guild IDs
}


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ImportantChannels {
    // All are channel IDs
    pub claiming: String,
    pub claim_list: String,
    pub ban_list: String,
    pub welcome: String,
    pub member_age_broadcast: String,
    pub cross_verify_log: String
}

impl Default for ImportantChannels {
    fn default() -> Self {
        Self {
            claiming: "0".to_string(),
            claim_list: "0".to_string(),
            ban_list: "0".to_string(),
            welcome: "0".to_string(),
            member_age_broadcast: "0".to_string(),
            cross_verify_log: "0".to_string(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ImportantRoles {
    // All are role IDs
    pub claimed: String,
    pub master: String,
    pub member: String,
    pub major_verified: String
}

impl Default for ImportantRoles {
    fn default() -> Self {
        Self {
            claimed: "0".to_string(),
            master: "0".to_string(),
            member: "0".to_string(),
            major_verified: "0".to_string(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Messages {
    // Raw JSON data of a message
    // TODO: create a better UI for this feature that doesn't involve raw JSON data
    // and probably also a better way to do this in the bot without having to parse json
    // or a string
    // we will need to find a better way to do this
    pub welcome: String,

    // this is just message content so it doesn't matter
    pub rules_accepted: String,
}

impl Default for Messages {
    fn default() -> Self {
        Self {
            welcome: "".to_string(),
            rules_accepted: "Welcome in!".to_string()
        }
    }
}

impl GuildEntry {
    /// Returns a new instance of GuildEntry.
    pub fn new(guild_id: &str) -> Self {
        Self {
            _id: guild_id.to_string(),
            features: 0,
            min_account_age: 24,
            claim_limit: 3,
            rules_buttons_swapped: false,
            ..Default::default()
        }
    }

    /// Fetches a guild from the "guilds" database using given guild ID
    pub async fn get_guild(couch_db: &CouchDB, id: &GuildId ) -> Option<Self> {
        let values: Vec<Value> = match couch_db.find(DataBase::GUILDS, {
            let mut query: Map<String, Value> = Map::new();
            query.insert("_id".to_string(), Value::String(id.to_string()));
            query
        }).await {
            Ok(v) => v,
            Err(e) => {
                println!("Error: {}", e);
                return None;
            }
        };
        match values.first() {
            Some(value) => serde_json::from_value(value.clone()).unwrap(),
            None => None
        }
    }

    /// Pushes a Guild to the "guilds" database.
    pub async fn push_guild(&mut self, couch_db: &CouchDB) -> Result<DocumentCreatedDetails, CouchError> {
        couch_db.create_or_update(DataBase::GUILDS, self).await
    }

    /// Returns all enabled Hanabot features in this guild.
    pub fn get_enabled_features(&self) -> Vec<FeaturesFlags> {
        let mut flags: Vec<FeaturesFlags> = Vec::new();
        let flags_int = utils::bitwise::integer_to_bitwise_flags(self.features as u32);
        for num in flags_int {
            match num {
                // Possible todo: find better way to do this that doesn't involve hardcoding values
                // or add a function to do this with any enum to bitwise.rs
                // not worth the effort rn though
                2 => flags.push(FeaturesFlags::Claiming),
                4 => flags.push(FeaturesFlags::AccountAgeCheck),
                8 => flags.push(FeaturesFlags::Welcoming),
                16 => flags.push(FeaturesFlags::BanList),
                32 => flags.push(FeaturesFlags::MemberAgeCheck),
                64 => flags.push(FeaturesFlags::Onboarding),
                128 => flags.push(FeaturesFlags::CrossVerify),
                _ => continue
            }
        }
        return flags
    }
}

/// Determines the channel ID and message ID of a Discord message.
// Putting it in this wrapper for now, may move later to another file.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MessageLocation {
    pub channel_id: ChannelId,
    pub message_id: MessageId
}

impl MessageLocation {
    /// Construct a new `MessageLocation` from a vector of two Strings in
    /// the format `[channel_id, message_id]`. The order of elements is important.
    pub fn new(ids: Vec<String>) -> Result<Self, String> {
        if ids.len() != 2 {
            return Err("The vector must be exactly two items: one channel ID and one message ID, in that order.".to_string())
        }

        // I believe the array is not stored as a set in CouchDB,
        // so we don't need to worry about the order of the elements
        // however if we do, then we can just check which ID is smaller
        // as the channel ID will always be a smaller int due to being created first.
        let channel_id: ChannelId = ChannelId::new(ids.get(0).expect("Couldn't get channel ID while constructing.").parse().unwrap());
        let message_id: MessageId = MessageId::new(ids.get(1).expect("Couldn't get message ID while constructing.").parse().unwrap());
        return Ok(Self{channel_id, message_id})
    }

    /// Returns the vector representation of this Message Location in
    /// the format `[channel_id, message_id]`. The IDs are Strings.
    ///
    /// This is used for storing in CouchDB.
    pub fn to_vec(&self) -> Vec<String> {
        return vec![self.channel_id.to_string(), self.message_id.to_string()]
    }
}
