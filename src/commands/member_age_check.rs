use std::collections::HashMap;
use poise::serenity_prelude::client::Context as SerenityContext;
use poise::serenity_prelude::{CacheHttp, ChannelId, ComponentInteraction, CreateActionRow, CreateAllowedMentions, CreateInputText, CreateInteractionResponse, CreateInteractionResponseMessage, CreateMessage, CreateModal, InputTextStyle, Interaction, Member, ModalInteraction, RoleId, UserId};

use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::{FeaturesFlags, GuildEntry};
use crate::utils::couch_db::CouchDB;

/// Show the member age check modal
pub async fn member_age_check_modal(ctx: SerenityContext, interaction: ComponentInteraction, args: Vec<&str>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let broadcast_channel: &String = &guild.important_channel_ids.member_age_broadcast.to_string();

    let age_option: CreateInputText = CreateInputText::new(InputTextStyle::Short, "Age", "AGE")
        .min_length(1)
        .max_length(3)
        .placeholder("e.g. 25")
        .required(true);
    let dob_option: CreateInputText = CreateInputText::new(InputTextStyle::Short, "Date of Birth (in EU format, dd/mm/yyyy)", "DOB")
        .min_length(1)
        .max_length(10)
        .placeholder("e.g. 9/8/1998")
        .required(true);

    let modal_components: Vec<CreateActionRow> = vec![CreateActionRow::InputText(age_option), CreateActionRow::InputText(dob_option)];

    let modal: CreateModal = CreateModal::new(format!("MEMBER_AGE_CHECK::ANSWER::BROADCAST_CID{broadcast_channel}"), "Enter your age and DOB.")
        .components(modal_components);

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Modal(modal)
    ).await.expect("Couldn't send!");
}

// sadly can't extract this message into its own function, since one takes a ModalInteraction and the other takes a ComponentInteraction
// well it might be possible but i can't be bothered right now
pub async fn member_age_check_answer(ctx: SerenityContext, interaction: ModalInteraction, values: &mut HashMap<String, String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let age: &String = values.get("AGE").unwrap();
    let dob: &String = values.get("DOB").unwrap();
    let user_id: &String = &interaction.user.id.to_string();
    let member: &Member = &interaction.member.as_ref().unwrap();

    let broadcast_message: CreateMessage = CreateMessage::new()
        .content(format!("<@{user_id}> is {age} years old and was born on {dob} (should be in EU format, dd/mm/yyyy)"))
        .allowed_mentions(CreateAllowedMentions::new().users(vec![UserId::new(user_id.parse().unwrap())]));

    let broadcast_channel_id: Option<String> = match GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await {
        Some(guild) => Some(guild.important_channel_ids.member_age_broadcast),
        None => None
    };

    if let Some(id) = &broadcast_channel_id {
        if !id.is_empty() {
            if let Ok(channels) = interaction.guild_id.unwrap().channels(ctx.http()).await {
                let channel_id: ChannelId = ChannelId::new(id.parse().unwrap());
                if let Some(channel) = channels.get(&channel_id) {
                    let _ = channel.send_message(ctx.http(), broadcast_message).await;
                }
            }
        }
    }

    member.add_role(ctx.http(), RoleId::new(guild.important_role_ids.member.parse().unwrap())).await.expect("Couldn't add role!");

    let confirm_message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("{}", match guild.messages.rules_accepted.is_empty() {
            true => { "Welcome in!".to_string() } // fallback just in case. This shouldn't happen under normal circumstances.
            false => { guild.messages.rules_accepted }
        }))
        .ephemeral(true);

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(confirm_message)
    ).await.expect("Couldn't send!");
}
