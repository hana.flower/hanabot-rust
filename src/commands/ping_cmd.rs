use poise::CreateReply;
use poise::serenity_prelude::{Mention, Mentionable, User};
use crate::Data;

type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data , Error>;

#[poise::command(slash_command)]
pub async fn ping(ctx: Context<'_>, user: Option<User>) -> Result<(), Error> {
    let unwrapped_user: User = if let Some(u) = user { u } else {
        ctx.say("Please specify a User!").await.expect("Couldn't send message");
        return Ok(())
    };
    let mentionable_user: Mention = unwrapped_user.mention();
    let message: CreateReply = CreateReply {
        content: Some(format!("{mentionable_user}")),
        embeds: vec![],
        attachments: vec![],
        ephemeral: None,
        components: None,
        allowed_mentions: None,
        reply: false,
        __non_exhaustive: (),
    };
    ctx.send(message).await.expect("Couldn't Ping!");
    Ok(())
}