use std::time::{SystemTime, UNIX_EPOCH};
use poise::CreateReply;
use poise::serenity_prelude::{ButtonStyle, CacheHttp, ChannelId, ComponentInteraction, ComponentInteractionDataKind, CreateActionRow, CreateButton, CreateEmbed, CreateInteractionResponse, CreateInteractionResponseMessage, CreateMessage, EditMessage, Guild, Member, Mentionable, User, UserId};
use poise::serenity_prelude::client::Context as SerenityContext;
use serde_json::{Map, Value};
use crate::commands::claim_cmds::claim_db_wrapper::{ClaimUser, ClaimWaitEntry, MasterUser, SubData, SubUser};
use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::GuildEntry;
use crate::utils::couch_db::CouchDB;

type Error = super::super::claim_cmds::Error;

/// This is the context type for the commands! Data is user defined in main.rs!
type Context<'a> = super::super::claim_cmds::Context<'a>;


#[poise::command(slash_command)]
#[allow(clippy::too_many_lines)]
pub async fn claim(
    ctx: Context<'_>,
    #[description = "Who would you like to claim?"] target: User,
    #[description = "Leave a note!"] note: Option<String>,
) -> Result<(), Error> {
    //Check if the author is trying to claim themselves.
    if ctx.author().id == target.id {
        let message: CreateReply = CreateReply {
            ephemeral: Some(true),
            components: None,
            allowed_mentions: None,
            reply: false,
            content: Some("You already belong to yourself silly <3".to_string()),
            embeds: vec![],
            attachments: vec![],
            __non_exhaustive: (),
        };
        ctx.send(message).await?;
        return Ok(());
    }

    let couch_db: &CouchDB = &ctx.data().couch_db; //Get reference to the couch_db client.

    let mut total_subs: Vec<String> = vec![];

    //Create a find query to fetch all entries with user_id: <author.id> and guild_id: <guild.id>
    let mut query: Map<String, Value> = Map::new();
    query.insert(
        "user_id".to_string(),
        Value::String(ctx.author().id.to_string()),
    );
    query.insert(
        "guild_id".to_string(),
        Value::String({
            // make sure the command is being run while in a guild.
            let guild: Option<Guild> = ctx.guild().map(|reference| reference.to_owned());
            if let Some(v) = guild { v.id.to_string() } else {
                let reply: CreateReply = CreateReply::default()
                    .ephemeral(true)
                    .content("Please use this command in a guild. ^-^");
                let _ = ctx.send(reply).await;
                return Ok(());
            }
        }),
    );

    let author_data: Vec<ClaimUser> = ClaimUser::get_user(couch_db, query).await?; // Fetch the author's data from DB if exists.
    // Verify if the author already had an entry in claim_db.
    if !author_data.is_empty() {
        match &author_data[0] {
            // Checking if the author is a sub.
            ClaimUser::SUB(_user) => {
                let message: CreateReply = CreateReply {
                    ephemeral: Some(true),
                    components: None,
                    allowed_mentions: None,
                    reply: false,
                    content: Some("Subs can't make acquisition contracts.".to_string()),
                    embeds: vec![],
                    attachments: vec![],
                    __non_exhaustive: (),
                };
                ctx.send(message).await?;
                return Ok(());
            }
            // Checking if the author already has the maximum number of claims he can have.
            ClaimUser::MASTER(master) => {
                // Number of maximum claims allowed by the server. Defaults to 3.
                let max_claims: usize = match GuildEntry::get_guild(couch_db, &ctx.guild_id().unwrap()).await {
                        Some(entry) => entry.claim_limit as usize,
                        None => 3
                    };
                if master.sub_id_list.len() > max_claims {
                    let message: CreateReply = CreateReply {
                        ephemeral: Some(true),
                        components: None,
                        allowed_mentions: None,
                        reply: false,
                        content: Some("You already have the maximum amount of subs allowed by this guild. Please disown one in order to make this acquisition.".to_string()),
                        embeds: vec![],
                        attachments: vec![],
                        __non_exhaustive: (),
                    };
                    ctx.send(message).await?;
                    return Ok(());
                }
                for entry in &master.sub_id_list {
                    total_subs.push(entry.clone());
                }
            }
        }
    }

    let mut query: Map<String, Value> = Map::new();
    query.insert("user_id".to_string(), Value::String(target.id.to_string()));
    query.insert(
        "guild_id".to_string(),
        Value::String(ctx.guild().unwrap().id.to_string()),
    );
    let target_data: Vec<ClaimUser> = ClaimUser::get_user(couch_db, query).await?;
    // Verify if the target has an existing entry in claim_db.
    if !target_data.is_empty() {
        let target_mention: String = target.mention().to_string();
        return match target_data.first().unwrap() {
            // Check if the target is a master.
            ClaimUser::MASTER(_) => {
                let message: CreateReply = CreateReply {
                    ephemeral: Some(true),
                    components: None,
                    allowed_mentions: None,
                    reply: false,
                    content: Some(format!(
                    "{target_mention} is a Master. You can't claim another Master."
                    )),
                    embeds: vec![],
                    attachments: vec![],
                    __non_exhaustive: (),
                };
                ctx.send(message).await?;
                Ok(())
            }
            // Check if the target already has a master.
            ClaimUser::SUB(_) => {
                let message: CreateReply = CreateReply {
                    ephemeral: Some(true),
                    components: None,
                    allowed_mentions: None,
                    reply: false,
                    content: Some(format!("{target_mention} already has a Master.")),
                    embeds: vec![],
                    attachments: vec![],
                    __non_exhaustive: (),
                };
                ctx.send(message).await?;
                Ok(())
            }
        };
    }

    total_subs.push(target.id.to_string());
    make_contract(ctx, target, note, total_subs).await?;
    Ok(())
}

///If Master with existing sub tries to make new acquisition.
///Make acquisition contract.
async fn make_contract(ctx: Context<'_>, target: User, note: Option<String>, sub_list: Vec<String>) -> Result<(), Error> {
    //Get user data.
    let author_name: String = ctx.author().global_name.clone().unwrap();
    let author_mention: String = ctx.author().mention().to_string();
    let target_name: String = match &target.global_name {
        Some(value) => value.to_string(),
        None => target.name.clone()
    };
    let target_mention: String = target.mention().to_string();
    let mut final_note: String = String::new();

    //Create the contract embed.
    let embed: CreateEmbed = match note {
        Some(note) => {
            final_note = note.clone();
            CreateEmbed::new()
                .description(format!("Acquisition contract initiated: {author_mention} -> {target_mention}"))
                .field(format!("{target_name}! Do you consent to being owned by {author_name}?"), format!("NOTE: {note}"), true)
        },
        None => {
            CreateEmbed::new()
                .description(format!("Acquisition contract initiated: {author_mention} -> {target_mention}"))
                .field(format!("{target_name}! Do you consent to being owned by {author_name}?"), "", true)
        }
    };


    let guild_id: String = ctx.guild().unwrap().id.to_string();
    let author_id: String = ctx.author().id.to_string();
    let target_id: String = target.id.to_string();

    //Create buttons.
    let yes_button: CreateButton = CreateButton::new(format!("CLAIM_CONTRACT_INTERACTION::{guild_id}::{author_id}::{target_id}::YES"))
        .label("Accept")
        .style(ButtonStyle::Primary);
    let no_button: CreateButton = CreateButton::new(format!("CLAIM_CONTRACT_INTERACTION::{guild_id}::{author_id}::{target_id}::NO"))
        .label("Deny")
        .style(ButtonStyle::Primary);

    //Create a message to reply with.
    let message: CreateReply = CreateReply::default()
        .embed(embed)
        .components(vec![CreateActionRow::Buttons(vec![yes_button, no_button])]);
    let active_contract_id: String = ctx.send(message).await?.message().await?.id.to_string();



    let mut wait_entry: ClaimWaitEntry = ClaimWaitEntry::new(
        active_contract_id,
        target_id,
        guild_id,
        author_id,
        final_note,
        sub_list,
    );
    wait_entry.push(&ctx.data().couch_db).await?;
    Ok(())
}

#[allow(clippy::too_many_lines)]
///Contract Button Logic!!
pub async fn contract_interaction(ctx: SerenityContext, mut interaction: ComponentInteraction) {
    if let ComponentInteractionDataKind::Button = interaction.data.kind {
        let id: &str = &interaction.data.custom_id;
        let args: Vec<&str> = id.split("::").collect::<Vec<&str>>();

        //Important Information.
        let interaction_guild: &str = args[1].trim();
        let claim_author_id: &str = args[2].trim();
        let claim_target_id: &str = args[3].trim();
        let interacter_id: &String = &interaction.user.id.to_string();

        //Creating a find query.
        let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
        let mut find_query: Map<String, Value> = Map::new();
        find_query.insert("_id".to_owned(), Value::String(format!("{interaction_guild}::{claim_author_id}::{claim_target_id}")));

        //Getting wait list doc.
        let entry: Vec<ClaimWaitEntry> = ClaimWaitEntry::get(&couch_db, find_query).await;
        if entry.is_empty() {
            println!("Entry is unavailable or corrupted.");
            return;
        }
        let mut entry: ClaimWaitEntry = entry.first().unwrap().clone();

        if !entry.sub_consent_ids.contains(interacter_id) {
            let _ = interaction.create_response(ctx.http, {
                CreateInteractionResponse::Message(
                    CreateInteractionResponseMessage::new()
                        .ephemeral(true)
                        .content("This is not your choice to make.")
                )
            }).await;
            return;
        }

        let button_pressed: &str = args[4].trim();
        match button_pressed {
            "YES" => {
                entry.set_consent(interacter_id, true);
                match entry.push(&couch_db).await {
                    Ok(_) => {}
                    Err(e) => {
                        println!("Error: {e}");
                        return;
                    }
                }
                
                //If all SubUsers have accepted.
                if entry.get_consent_state() {
                    //Search the CLAIMS database for any entry with the author's id.
                    let user_db_entry: Vec<ClaimUser> = ClaimUser::get_user(&couch_db, {
                        let mut query: Map<String, Value> = Map::new();
                        query.insert("_id".to_owned(), Value::String(format!("CLAIM::{interaction_guild}::{claim_author_id}")));
                        query
                    }).await.unwrap_or_else(|e| {
                        println!("{e}");
                        vec![]
                    });
                    
                    let master_user: MasterUser = if user_db_entry.is_empty() {
                        let sub_list: Vec<String> = vec![claim_target_id.to_owned()];
                        MasterUser::new(interaction_guild, claim_author_id, true, sub_list)
                    } else {
                        let master_data: &MasterUser = match user_db_entry.first().unwrap() {
                            ClaimUser::MASTER(user) => {
                                user
                            },
                            ClaimUser::SUB(_) => {
                                println!("Error: Master is a sub? >~<");
                                return;
                            }
                        };

                        let mut sub_list: Vec<String> = master_data.sub_id_list.clone();
                        sub_list.push(claim_target_id.to_owned());
                        MasterUser::new(interaction_guild, claim_author_id, true, sub_list)
                    };

                    //Creating the MasterUser ClaimUser and pushing it to CLAIMS database.
                    match ClaimUser::MASTER(master_user).push_user(&couch_db).await {
                        Ok(_) => {}
                        Err(e) => {
                            println!("Error: {e}");
                            return;
                        }
                    }

                    //Getting current EPOCH time for timestamp.
                    let current_time = match SystemTime::now().duration_since(UNIX_EPOCH) {
                        Ok(duration) => duration.as_secs().to_string(),
                        Err(e) => e.to_string()
                    };


                    //Creating a Sub_User struct for new sub.
                    let sub_user: SubUser = SubUser::new(
                        interaction_guild,
                        claim_target_id,
                        false,
                        SubData {
                            master_id: claim_author_id.to_string(),
                            note: entry.note.clone(),
                            epoch_timestamp: current_time,
                            contract_id: String::new()
                        }
                    );


                    //Deleting waitlist entry.
                    match entry.delete(&couch_db).await {
                        Ok(()) => {}
                        Err(e) => {
                            println!("Error: {e}");
                            return;
                        }
                    }

                    //Get the claim users.
                    let author_id: UserId = UserId::new(str::parse(claim_author_id).unwrap_or_else({|e| {
                        println!("{e}");
                        0
                    }}));
                    let target_id: UserId = UserId::new(str::parse(claim_target_id).unwrap_or_else({|e| {
                        println!("{e}");
                        0
                    }}));
                    let author: Member = interaction.guild_id.unwrap().member(ctx.http(), author_id).await.unwrap_or_else(|e| {
                        println!("Error: {e}");
                        Member::default()
                    });
                    let target: Member = interaction.guild_id.unwrap().member(ctx.http(), target_id).await.unwrap_or_else(|e| {
                        println!("Error: {e}");
                        Member::default()
                    });

                    //Check if there's a note.
                    let mut note: Option<String> = None;
                    if entry.note != *"" {
                        note = Some(entry.note);
                    }

                    //Getting string values.
                    let author_mention: String = author.mention().to_string();
                    let author_name: String = author.display_name().to_string();
                    let target_mention: String = target.mention().to_string();
                    let target_name: String = target.display_name().to_string();

                    //Create the contract complete embed.
                    let embed: CreateEmbed = match note {
                        Some(note) => {
                            CreateEmbed::new()
                                .description(format!("Acquisition contract complete: {author_mention} -> {target_mention}"))
                                .field(format!("{target_name} is now owned by {author_name}!"), format!("NOTE: {note}"), true)
                        },
                        None => {
                            CreateEmbed::new()
                                .description(format!("Acquisition contract complete: {author_mention} -> {target_mention}"))
                                .field(format!("{target_name} is now owned by {author_name}!"), "", true)
                        }
                    };

                    //Create buttons.
                    let yes_button: CreateButton = CreateButton::new("YES")
                        .label("Accept")
                        .style(ButtonStyle::Primary)
                        .disabled(true);
                    let no_button: CreateButton = CreateButton::new("NO")
                        .label("Deny")
                        .style(ButtonStyle::Primary)
                        .disabled(true);

                    //Getting the contract message id. If none is specified, then defaults to "".
                    let contract_channel_id: Option<String> = match GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await {
                        Some(guild) => Some(guild.important_channel_ids.claiming),
                        None => None
                    };

                    //Create a message to reply with.
                    let edit_message: EditMessage = EditMessage::new()
                        .components(vec![CreateActionRow::Buttons(vec![yes_button, no_button])])
                        .embed(embed.clone());

                    let _ = interaction.message.edit(ctx.http.clone(), edit_message).await;

                    if let Some(id) = &contract_channel_id {
                        if !id.is_empty() {
                            if let Ok(channels) = interaction.guild_id.unwrap().channels(ctx.http()).await {
                                let channel_id: ChannelId = ChannelId::new(id.parse().unwrap());
                                if let Some(channel) = channels.get(&channel_id) {
                                    let _ = channel.send_message(ctx.http(), CreateMessage::new().embed(embed)).await;
                                }
                            } 
                        }
                    }


                    //Pushing said sub to data_base.
                    match ClaimUser::SUB(sub_user).push_user(&couch_db).await {
                        Ok(_) => {}
                        Err(e) => {
                            println!("Error: {e}");
                            return;
                        }
                    }


                    // Contract initiated message.
                    let _ = interaction.create_response(ctx.http, {
                        CreateInteractionResponse::Message(
                            CreateInteractionResponseMessage::new()
                                .content("Contract Accepted! *This contract is now in order.*")
                                .ephemeral(true)
                        )
                    }).await;
                    return;
                }

                // Claims accepted messages.
                let _ = interaction.create_response(ctx.http, {
                    CreateInteractionResponse::Message(
                        CreateInteractionResponseMessage::new()
                            .content("Contract Accepted! *you can change your choice as long as the contract is pending.*")
                            .ephemeral(true)
                    )
                }).await;
            }
            // If user declines...
            "NO" => {
                let _ = interaction.create_response(
                    ctx.http,
                    CreateInteractionResponse::Message(
                        CreateInteractionResponseMessage::new()
                            .content("Contract Denied! *you can change your choice as long as the contract is pending.*")
                            .ephemeral(true)
                    )
                ).await;
                entry.set_consent(interacter_id, false);
                match entry.push(&couch_db).await {
                    Ok(_) => {}
                    Err(e) => {
                        println!("Error: {e}");
                    }
                }
            }
            _ => {
                println!("Invalid Interaction: {id}");
            }
        }
    }
}
