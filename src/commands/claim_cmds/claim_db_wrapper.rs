use couch_rs::{CouchDocument, document::TypedCouchDocument, error::CouchError};
use couch_rs::error::CouchResult;
use couch_rs::types::document::DocumentCreatedDetails;
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

use crate::utils::couch_db::{CouchDB, DataBase};

#[derive(Serialize, Deserialize, Clone, Debug, CouchDocument, PartialEq)]
pub struct MasterUser {
    pub _id: String,
    _rev: String,
    pub guild_id: String,
    pub user_id: String,
    pub is_master: bool,
    pub sub_id_list: Vec<String>,
}

impl MasterUser {
    ///Create a new `MasterUser` object!
    pub fn new(guild_id: &str, user_id: &str, is_master: bool, sub_id_list: Vec<String>) -> Self {
        Self {
            _id: format!("CLAIM::{guild_id}::{user_id}"),
            _rev: String::new(),
            guild_id: guild_id.to_string(),
            user_id: user_id.to_string(),
            is_master,
            sub_id_list
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct SubData {
    pub master_id: String,
    pub note: String,
    pub epoch_timestamp: String,
    pub contract_id: String
}
#[derive(Serialize, Deserialize, Clone, Debug, CouchDocument, PartialEq)]
pub struct SubUser {
    pub _id: String,
    pub _rev: String,
    pub guild_id: String,
    pub user_id: String,
    pub is_master: bool,
    pub sub_data: SubData,
}

impl SubUser {
    ///Create a new `SubUser` object!
    pub fn new(guild_id: &str, user_id: &str, is_master: bool, sub_data: SubData) -> Self {
        Self {
            _id: format!("CLAIM::{guild_id}::{user_id}"),
            _rev: String::new(),
            guild_id: guild_id.to_string(),
            user_id: user_id.to_string(),
            is_master,
            sub_data
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[allow(clippy::upper_case_acronyms)]
pub enum ClaimUser {
    MASTER(MasterUser),
    SUB(SubUser),
}

impl ClaimUser {
    ///Fetches a list of users from ``CLAIMS`` database using a given selector query.
    pub async fn get_user(
        couch_db: &CouchDB,
        query: Map<String, Value>,
    ) -> Result<Vec<ClaimUser>, CouchError> {
        let user_list: Vec<Value> = couch_db.find(DataBase::CLAIMS, query).await?;
        let mut user_ob_list: Vec<ClaimUser> = vec![];
        if user_list.is_empty() {
            return Ok(user_ob_list);
        }
        for user in user_list {
            if serde_json::from_value(
                user.get("is_master")
                    .expect("Couldn't find field is_master.")
                    .clone(),
            )
                .expect("Couldn't Deserialize: ")
            {
                let master: MasterUser = serde_json::from_value(user).expect("Couldn't Deserialize: ");
                user_ob_list.push(ClaimUser::MASTER(master));
            } else {
                let sub: SubUser = serde_json::from_value(user).expect("Couldn't Deserialize: ");
                user_ob_list.push(ClaimUser::SUB(sub));
            }
        }
        Ok(user_ob_list)
    }

    ///Pushes a `ClaimUser` to the ``CLAIMS`` database.
    pub async fn push_user(&self, couch_db: &CouchDB) -> Result<DocumentCreatedDetails, CouchError> {
        match self {
            ClaimUser::MASTER(user) => {
                let mut user: MasterUser = user.clone();
                couch_db
                    .create_or_update(DataBase::CLAIMS, &mut user)
                    .await
            }
            ClaimUser::SUB(user) => {
                let mut user: SubUser = user.clone();
                couch_db
                    .create_or_update(DataBase::CLAIMS, &mut user)
                    .await
            }
        }
    }

    /// Deletes `THIS` doc from the database.
    pub async fn pop_user(&self, couch_db: &CouchDB) -> CouchResult<()> {
        couch_db.delete_doc( {
            match self {
                ClaimUser::MASTER(v) => &v._id,
                ClaimUser::SUB(v) => &v._id
            }
        } , DataBase::CLAIMS).await
    }
}


#[derive(Serialize, Deserialize, CouchDocument, Clone, Debug)]
pub struct ClaimWaitEntry {
    pub _id: String,
    _rev: String,
    pub contract_id: String,
    pub sub_id: String,
    pub guild_id: String,
    pub master_id: String,
    pub note: String,
    pub sub_consent_ids: Vec<String>,
    pub sub_consent_values: Vec<bool>
}

impl ClaimWaitEntry {
    ///Create a new Wait List entry doc.
    pub fn new(contract_id: String, target_id: String, guild_id: String, master_id: String, note: String, sub_list: Vec<String>) -> Self {
        let mut sub_consent_values: Vec<bool> = vec![];
        if !sub_list.is_empty() {
            for _i in 0..sub_list.len() {
                sub_consent_values.push(false);
            }
        }
        Self {
            _id: format!("{guild_id}::{master_id}::{target_id}"),
            _rev: String::new(),
            contract_id,
            sub_id: target_id,
            guild_id,
            master_id,
            note,
            sub_consent_ids: sub_list,
            sub_consent_values
        }
    }

    ///Change the consent state of one `sub_entry`.
    pub fn set_consent(&mut self, target_id: &str, state: bool) -> Self{
        let sub_list: &Vec<String> = &self.sub_consent_ids;
        for (idx, sub_id) in sub_list.iter().enumerate() {
            if sub_id == target_id { self.sub_consent_values[idx] = state; }
        }
        self.to_owned()
    }

    ///Get the consent state of one entry.
    pub fn get_consent_state(&self) -> bool {
        let consent_states: &Vec<bool> = &self.sub_consent_values;
        let mut final_state: bool = true;
        for state in consent_states {
            if !state { final_state = false; break; }
        }
        final_state
    }

    ///Push the doc into the `CLAIM_WAITLIST` db.
    pub async fn push(&mut self, couch_db: &CouchDB) -> Result<DocumentCreatedDetails, CouchError>{
        couch_db.create_or_update(DataBase::CLAIM_WAITLIST, self).await
    }

    ///Delete the doc from the `CLAIM_WAITLIST` db.
    pub async fn delete(&self, couch_db: &CouchDB) -> CouchResult<()> {
        couch_db.delete_doc(self._id.as_str(), DataBase::CLAIM_WAITLIST).await
    }

    ///Gets a specified doc from the `CLAIM_WAITLIST` db.
    pub async fn get(couch_db: &CouchDB, query: Map<String, Value>) -> Vec<Self> {
        let mut _docs: Vec<Value> = vec![];
        match couch_db.find(DataBase::CLAIM_WAITLIST, query).await {
            Ok(dataset) => {
                _docs = dataset;
            }
            Err(e) => {
                println!("{e}");
                return vec![];
            }
        }
        if _docs.is_empty() {
            println!("doc len 0");
            return vec![];
        }
        let mut output: Vec<ClaimWaitEntry> = vec![];
        for doc in _docs {
            let entry: ClaimWaitEntry = match serde_json::from_value(doc) {
                Ok(v) => v,
                Err(e) => {
                    println!("{e}");
                    return vec![];
                }
            };
            output.push(entry);
        }
        output
    }
}
