use std::time::UNIX_EPOCH;
use couch_rs::{document::TypedCouchDocument, error::CouchError, types::document::DocumentCreatedDetails, CouchDocument};
use poise::serenity_prelude::{GuildId, UserId};
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

use crate::utils::couch_db::{CouchDB, DataBase};


/// The Schema for the default entry to the Delete Queue.
#[derive(Serialize, Deserialize, CouchDocument, Clone, Debug)]
struct ClaimDeleteQueueEntry {
    /// The Doc's unique ID.
    /// DeleteQueueEntry::new() generates it as <entry_type::guild_id::user_id>
    pub _id: String,
    _rev: String,
    entry_type: String,
    /// The guild's id.
    pub guild_id: String,
    /// The user's id
    pub user_id: String,
    /// Time stamp of when the entry is being made.
    pub time_stamp: u64
}



impl ClaimDeleteQueueEntry {
    /// Returns a new instance of the `DeleteQueueEntry` struct.
    pub fn new(guild_id: GuildId, user_id: UserId) -> Self {
        let entry_type: &str = "CLAIM_DELETE";
        let guild_id: String = guild_id.to_string();
        let user_id: String = user_id.to_string();
        Self {
            _id: format!("{entry_type}::{guild_id}::{user_id}"),
            _rev: String::new(),
            entry_type: entry_type.to_string(),
            guild_id,
            user_id,
            time_stamp: UNIX_EPOCH.elapsed().unwrap().as_secs()
        }
    }

    /// Returns the type of the `delete_queue` entry.
    pub fn get_entry_type(&self) -> &str {
        &self.entry_type
    }

    /// Pushes the doc to the `DELETE_QUEUE` database.
    pub async fn push(&mut self, couch_db: &CouchDB) -> Result<DocumentCreatedDetails, CouchError> {
        couch_db.create_or_update(DataBase::DELETE_QUEUE, self).await
    }

    /// Gets a list of entries with a given id as query.
    pub async fn find(couch_db: &CouchDB, id: &str) -> Option<Self> {
        let got: Vec<Value> = match couch_db.find(DataBase::DELETE_QUEUE, {
            let mut query: Map<String, Value> = Map::new();
            query.insert("_id".to_owned(), Value::String(id.to_string()));
            query
        }).await {
            Ok(this) => this,
            Err(e) => {
                println!("{e}");
                return None;
            }
        };

        let output: Option<ClaimDeleteQueueEntry> = match got.first() {
            Some(v) => serde_json::from_value(v.clone()).unwrap(),
            None => None
        };
        output
    }
}
