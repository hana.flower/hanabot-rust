use poise::CreateReply;
use poise::serenity_prelude::{ChannelId, CreateEmbed, EditMessage, Embed, MessageId, User};
use serde_json::{Map, Value};

use crate::commands::claim_cmds::claim_db_wrapper::ClaimUser;
use crate::CouchDB;
use crate::guild_settings::guilds_db_wrapper::GuildEntry;

type Error = super::super::claim_cmds::Error;
type Context<'a> = super::super::claim_cmds::Context<'a>;


/// Edit the note assigned to a claim.
#[poise::command(slash_command)]
pub async fn edit(ctx: Context<'_>, #[description = "Which claim do you wish to edit?"] target: User, #[description = "New note: <Leave empty if wish to remove note completely.>"] note: Option<String>) -> Result<(), Error> {
    let couch_db: CouchDB = ctx.data().couch_db.clone(); // The CouchDB client.

    // Checking if the author is in the Claim_DB.
    if let Some(user) = ClaimUser::get_user(&couch_db, {
        let mut query: Map<String, Value> = Map::new();
        query.insert("user_id".to_string(), Value::String(ctx.author().id.to_string()));
        query.insert("guild_id".to_string(), Value::String(ctx.guild_id().unwrap().to_string()));
        query
    }).await?.first() {
        match user { 
            ClaimUser::MASTER(u) => {
                if !u.sub_id_list.contains(&target.id.to_string()) {
                    // Send a decline message to the use since the target is not their sub.
                    let _ = ctx.send(DeclineMessage::new("The specified target doesn't belong to you.").message).await;
                    return Ok(());
                }
            }
            ClaimUser::SUB(_) => {
                // Send decline message to user since a dub can't edit claim.
                let _ = ctx.send(DeclineMessage::new("Subs can't edit claim statements").message).await;
                return Ok(());
            }
        }
    } else {
        // Send a decline message to the user since they don't own a sub.
        let _ = ctx.send(DeclineMessage::new("You are not a master").message).await;
        return Ok(());
    };


    // Checking if the target is in the Claim_DB.
    if let Some(user) = ClaimUser::get_user(&couch_db, {
        let mut query: Map<String, Value> = Map::new();
        query.insert("user_id".to_string(), Value::String(target.id.to_string()));
        query.insert("guild_id".to_string(), Value::String(ctx.guild_id().unwrap().to_string()));
        query
    }).await?.first() {
        if let ClaimUser::SUB(mut sub_user) = user.clone() {
            if let Some(note) = note.clone() { sub_user.sub_data.note = note; }
            else { sub_user.sub_data.note = String::new() }
            match ClaimUser::SUB(sub_user.clone()).push_user(&couch_db).await {
                Ok(_) => {}
                Err(error) => {
                    println!("Error: {error}");
                    return Err(error.into());
                }
            };
            if let Some(claim_guild) = GuildEntry::get_guild(&couch_db, &ctx.guild_id().unwrap()).await {
                match ctx.guild_id().unwrap().channels(ctx.http()).await {
                    Ok(channels) => {
                        if let Some(channel) = channels.get(&ChannelId::new(claim_guild.important_channel_ids.claim_list.parse().unwrap_or(0))) {
                            if let Ok(mut message) = channel.message(ctx.http(), MessageId::new(sub_user.sub_data.contract_id.parse().unwrap_or(0))).await {
                                let old_embed: Embed = message.embeds.first().unwrap().to_owned();
                                let new_embed: CreateEmbed = match note { 
                                    Some(note) => {
                                        CreateEmbed::new()
                                            .description(old_embed.description.unwrap())
                                            .field(old_embed.fields.first().unwrap().name.clone(), format!("NOTE: {note}"), true)
                                    }
                                    None => {
                                        CreateEmbed::new()
                                            .description(old_embed.description.unwrap())
                                            .field(old_embed.fields.first().unwrap().name.clone(), String::new(), true)
                                    }
                                };
                                let _ = message.edit(ctx.http(), EditMessage::new().embed(new_embed)).await;
                            } else {
                                println!("Error: specified claim message doesn't exist.");
                                return Ok(())
                            }
                        } else {
                            println!("Error: specified claim channel doesn't exist.");
                            return Ok(())
                        };
                    }
                    Err(error) => {
                        println!("Error: {error}");
                        return Err(error.into());
                    }
                }
            }
        }
    } else {
        // Send a decline message to the user since the target is not in DB.
        let _ = ctx.send(DeclineMessage::new("target wasn't found in DB.").message).await;
        println!("user \"{}\" is not in db", target.name);
        return Ok(());
    };
    Ok(())
}

/// A simple wrapper over `CreateReply` so that we don't have to construct em everytime over and over.
struct DeclineMessage {
    message: CreateReply
}

impl DeclineMessage {
    /// Creates a new instance of `DeclineMessage`.
    fn new(reason: &str) -> Self {
        Self {
            message: { CreateReply { ephemeral: Some(true), content: Some(format!("Access Denied: {reason}")), ..Default::default() } }
        }
    }
}