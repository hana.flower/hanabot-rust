use poise::serenity_prelude::client::Context as SerenityContext;
use poise::serenity_prelude::{CacheHttp, ComponentInteraction, CreateInteractionResponse, CreateInteractionResponseMessage, Member, RoleId};

use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::{FeaturesFlags, GuildEntry};
use crate::utils::couch_db::CouchDB;

pub async fn rules_accept(ctx: SerenityContext, interaction: ComponentInteraction, args: Vec<&str>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    if guild.get_enabled_features().contains(&FeaturesFlags::MemberAgeCheck) {
        crate::commands::member_age_check::member_age_check_modal(ctx, interaction, args).await;
    }
    else {
        let member: &Member = &interaction.member.as_ref().unwrap();
        member.add_role(ctx.http(), RoleId::new(guild.important_role_ids.member.parse().unwrap())).await.expect("Couldn't add role!");
        let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
            .content(format!("{}", match guild.messages.rules_accepted.is_empty() {
                true => { "Welcome in!".to_string() } // fallback just in case. This shouldn't happen under normal circumstances.
                false => { guild.messages.rules_accepted }
            }))
            .ephemeral(true);

        interaction.create_response(
            ctx.http,
            CreateInteractionResponse::Message(message)
        ).await.expect("Couldn't send!");
    }
}
