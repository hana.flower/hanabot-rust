use std::collections::HashMap;
use poise::serenity_prelude::{ButtonStyle, ComponentInteraction, CreateActionRow, CreateAllowedMentions, CreateButton, CreateInputText, CreateInteractionResponse, CreateInteractionResponseMessage, CreateModal, CreateSelectMenu, CreateSelectMenuKind, InputTextStyle, ModalInteraction};
use poise::serenity_prelude::client::Context as SerenityContext;

use crate::commands::settings_cmds::{gen_footer_row, gen_single_role_menu, gen_single_channel_menu, GENERIC_ROLE_MENU_ADVICE, GENERIC_ROLE_SELECTION_ADVICE, GENERIC_ROLE_SELECTION_REMOVAL_ADVICE, GENERIC_CHANNEL_MENU_ADVICE};
use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::GuildEntry;
use crate::utils::couch_db::CouchDB;

pub(super) async fn claiming_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let claimed_role_button: CreateButton = CreateButton::new("SETTINGS::CLAIMING::CLAIMED_ROLE".to_string())
        .label("Set claimed role")
        .style(ButtonStyle::Primary);
    let master_role_button: CreateButton = CreateButton::new("SETTINGS::CLAIMING::MASTER_ROLE".to_string())
        .label("Set master role")
        .style(ButtonStyle::Primary);
    let claim_list_button: CreateButton = CreateButton::new("SETTINGS::CLAIMING::CLAIM_LIST".to_string())
        .label("Set claim list channel")
        .style(ButtonStyle::Primary);
    let claiming_button: CreateButton = CreateButton::new("SETTINGS::CLAIMING::CLAIMING_CHANNEL".to_string())
        .label("Set claiming channel")
        .style(ButtonStyle::Primary);
    let claim_limit_button: CreateButton = CreateButton::new("SETTINGS::CLAIMING::CLAIM_LIMIT".to_string())
        .label("Set claim limit")
        .style(ButtonStyle::Primary);

    let row1: CreateActionRow = CreateActionRow::Buttons(vec![
        claimed_role_button,
        master_role_button,
        claim_list_button,
        claiming_button,
        claim_limit_button]);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content("## Hanabot settings  >  Claiming\n\nChange claiming settings.")
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_claimed_role_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_role_menu(&guild.important_role_ids.claimed);

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::CLAIMING::CLAIMED_ROLE::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a role");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Claiming  >  Set claimed role\n\nSet the role given to members when they are claimed by someone else.{GENERIC_ROLE_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_claimed_role_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_role_ids.claimed = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Claimed role set to <@&{id}>.{GENERIC_ROLE_SELECTION_ADVICE}"))
                .ephemeral(true)
                .allowed_mentions(CreateAllowedMentions::new()); // Disable all mentions.
        }
        None => {
            guild.important_role_ids.claimed = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Claimed role removed.{GENERIC_ROLE_SELECTION_REMOVAL_ADVICE}"))
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_master_role_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_role_menu(&guild.important_role_ids.master);

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::CLAIMING::MASTER_ROLE::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a role");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Claiming  >  Set master role\n\nSet the role given to members when they claim someone else.{GENERIC_ROLE_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_master_role_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_role_ids.master = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Master role set to <@&{id}>.{GENERIC_ROLE_SELECTION_ADVICE}"))
                .ephemeral(true)
                .allowed_mentions(CreateAllowedMentions::new()); // Disable all mentions.
        }
        None => {
            guild.important_role_ids.master = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Master role removed.{GENERIC_ROLE_SELECTION_REMOVAL_ADVICE}"))
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_claim_list_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_channel_menu(&guild.important_channel_ids.claim_list);

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::CLAIMING::CLAIM_LIST::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a channel");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Claiming  >  Set claim list channel\n\nSet the channel where claims are broadcast in, or no channel if you do not want them to be broadcast.{GENERIC_CHANNEL_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_claim_list_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_channel_ids.claim_list = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Claim list channel set to <#{id}>."))
                .ephemeral(true);
        }
        None => {
            guild.important_channel_ids.claim_list = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Claim list channel removed. Claims will not be broadcast anywhere.")
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_claiming_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_channel_menu(&guild.important_channel_ids.claiming);

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::CLAIMING::CLAIMING_CHANNEL::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a channel");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Claiming  >  Set claiming channel\n\nSet the channel where people are allowed to claim each other in, or no channel if you want the command to be usable everywhere.{GENERIC_CHANNEL_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_claiming_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_channel_ids.claiming = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Claiming channel set to <#{id}>."))
                .ephemeral(true);
        }
        None => {
            guild.important_channel_ids.claiming = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Claiming channel removed. Users can claim each other in any channel now.")
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_claim_limit_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let claim_limit_option: CreateInputText = CreateInputText::new(InputTextStyle::Short, "Limit (max. 50)", "SETTINGS::CLAIMING::CLAIM_LIMIT::SET")
        .min_length(1)
        .max_length(2)
        .placeholder("e.g. 3")
        .value(&guild.claim_limit.to_string())
        .required(true);

    let modal_components: Vec<CreateActionRow> = vec![CreateActionRow::InputText(claim_limit_option)];

    let modal: CreateModal = CreateModal::new("SETTINGS::CLAIMING::CLAIM_LIMIT::SET", "Enter the max. number of claims per master.")
        .components(modal_components);
    
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Modal(modal)
    ).await.expect("Couldn't send!");
}

pub(super) async fn claiming_claim_limit_set(ctx: SerenityContext, interaction: ModalInteraction, values: &mut HashMap<String, String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get("SETTINGS::CLAIMING::CLAIM_LIMIT::SET") {
        Some(claim_limit) => {
            if let Ok(claim_limit) = claim_limit.parse() {
                // The claim limit must be between 1 and 25. If you're going above 25, you may want to get a harem bot instead.
                if claim_limit < 1 || claim_limit > 25 {
                    message = CreateInteractionResponseMessage::new()
                        .content("Please set a value between 1 and 25.".to_string())
                        .ephemeral(true);
                }
                else {
                    guild.claim_limit = claim_limit;
                    guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
                    message = CreateInteractionResponseMessage::new()
                        .content(format!("Claim limit set to {claim_limit}."))
                        .ephemeral(true);
                }

            } else {
                // The ID was not an int value.
                message = CreateInteractionResponseMessage::new()
                    .content("Please only enter an integer (a whole number) between 1 and 50.".to_string())
                    .ephemeral(true);
            }
        }
        None => {
            panic!("Received no claim limit value.");
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}
