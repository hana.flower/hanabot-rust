use poise::serenity_prelude::{ButtonStyle, ComponentInteraction, CreateActionRow, CreateAllowedMentions, CreateButton, CreateInteractionResponse, CreateInteractionResponseMessage, CreateSelectMenu, CreateSelectMenuKind};
use poise::serenity_prelude::client::Context as SerenityContext;

use crate::commands::settings_cmds::{gen_footer_row, gen_single_role_menu, GENERIC_CHANNEL_MENU_ADVICE, GENERIC_ROLE_SELECTION_REMOVAL_ADVICE, GENERIC_ROLE_MENU_ADVICE, GENERIC_ROLE_SELECTION_ADVICE};
use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::GuildEntry;
use crate::utils::couch_db::CouchDB;

// The help links are currently placeholders, since I haven't gotten to documenting cross-verification yet.
pub(super) async fn cross_verify_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let major_role_button: CreateButton = CreateButton::new("SETTINGS::CROSS_VERIFY::MAJOR_ROLE".to_string())
        .label("Set 18+ verified role")
        .style(ButtonStyle::Primary);
    let log_channel_button: CreateButton = CreateButton::new("SETTINGS::CROSS_VERIFY::LOG_CHANNEL".to_string())
        .label("Set verification log channel")
        .style(ButtonStyle::Primary);
    let verify_type_button: CreateButton = CreateButton::new("SETTINGS::CROSS_VERIFY::VERIFY_TYPE".to_string())
        .label("Modify verify steps")
        .style(ButtonStyle::Primary);

    let row1: CreateActionRow = CreateActionRow::Buttons(vec![
        major_role_button,
        log_channel_button,
        verify_type_button
    ]);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content("## Hanabot settings  >  Cross-verification\n\nChange cross-verification settings. Managing guild partnerships should be done by using commands.")
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Cross-Verification".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn cross_verify_major_role_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_role_menu(&guild.important_role_ids.major_verified);

    let major_role_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::CROSS_VERIFY::MAJOR_ROLE::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a role");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(major_role_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Cross-verification  >  Set 18+ verified role\n\nSet the role given to members when they are verified as 18+.{GENERIC_ROLE_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Cross-Verification".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn cross_verify_major_role_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_role_ids.major_verified = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("18+ verified role set to <@&{id}>.{GENERIC_ROLE_SELECTION_ADVICE}"))
                .ephemeral(true)
                .allowed_mentions(CreateAllowedMentions::new()); // Disable all mentions.
        }
        None => {
            guild.important_role_ids.major_verified = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("18+ verified role removed.{GENERIC_ROLE_SELECTION_REMOVAL_ADVICE}"))
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn cross_verify_log_channel_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_role_menu(&guild.important_channel_ids.cross_verify_log);

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::CROSS_VERIFY::LOG_CHANNEL::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a channel");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Cross-verification  >  Set verification log channel\n\nSet the channel where verifications are broadcast in, or no channel if you do not want them to be broadcast. **It is highly recommended to set one in order to verify who verified.**. This does not currently log any information other than who verified and when they verified.{GENERIC_CHANNEL_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Cross-Verification".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn cross_verify_log_channel_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_channel_ids.cross_verify_log = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Verification log channel set to <#{id}>."))
                .ephemeral(true);
        }
        None => {
            guild.important_channel_ids.cross_verify_log = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Verification log channel removed. Verifications will not be logged anywhere.")
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn cross_verify_verify_type_menu(ctx: SerenityContext, interaction: ComponentInteraction, _values: &Vec<String>) {
    let message = CreateInteractionResponseMessage::new()
        .content("This is not currently implemented.")
        .ephemeral(true);

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}