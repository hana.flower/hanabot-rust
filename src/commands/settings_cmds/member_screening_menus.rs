use std::collections::HashMap;
use poise::serenity_prelude::{ButtonStyle, ComponentInteraction, CreateActionRow, CreateButton, CreateInputText, CreateInteractionResponse, CreateInteractionResponseMessage, CreateModal, CreateSelectMenu, CreateSelectMenuKind, InputTextStyle, ModalInteraction};
use poise::serenity_prelude::client::Context as SerenityContext;

use crate::commands::settings_cmds::{gen_footer_row, gen_single_channel_menu, GENERIC_CHANNEL_MENU_ADVICE};
use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::GuildEntry;
use crate::utils::couch_db::CouchDB;

pub(super) mod rules_menus;

pub(super) async fn member_screening_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let account_age_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::ACCOUNT_AGE".to_string())
        .label("Set minimum account age")
        .style(ButtonStyle::Primary);
    let reinvite_url_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::REINVITE_URL".to_string())
        .label("Set re-invite URL")
        .style(ButtonStyle::Primary);
    let rules_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::RULES".to_string())
        .label("Rules settings")
        .style(ButtonStyle::Primary);
    let member_age_check_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::MEMBER_AGE_CHECK".to_string())
        .label("Set member age log channel")
        .style(ButtonStyle::Primary);

    let row1: CreateActionRow = CreateActionRow::Buttons(vec![
        account_age_button,
        reinvite_url_button,
        rules_button,
        member_age_check_button]);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content("## Hanabot settings  >  Member screening\n\nChange member screening settings.")
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn member_screening_account_age_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let account_age_option: CreateInputText = CreateInputText::new(InputTextStyle::Short, "Age in hours", "SETTINGS::MEMBER_SCREENING::ACCOUNT_AGE::SET")
        .min_length(1)
        .max_length(5)
        .placeholder("e.g. 24")
        .value(&guild.min_account_age.to_string())
        .required(true);

    let modal_components: Vec<CreateActionRow> = vec![CreateActionRow::InputText(account_age_option)];

    let modal: CreateModal = CreateModal::new("SETTINGS::MEMBER_SCREENING::ACCOUNT_AGE::SET", "How many hours old do accounts have to be?")
        .components(modal_components);

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Modal(modal)
    ).await.expect("Couldn't send!");
}

pub(super) async fn member_screening_account_age_set(ctx: SerenityContext, interaction: ModalInteraction, values: &mut HashMap<String, String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get("SETTINGS::MEMBER_SCREENING::ACCOUNT_AGE::SET") {
        Some(min_age) => {
            if let Ok(min_age) = min_age.parse() {
                guild.min_account_age = min_age;
                guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
                message = CreateInteractionResponseMessage::new()
                    .content(format!("Minimum account age set to {min_age} hours."))
                    .ephemeral(true);
            } else {
                // The input was not an int value.
                message = CreateInteractionResponseMessage::new()
                    .content("Please only enter an integer (a whole number).".to_string())
                    .ephemeral(true);
            }
        }
        None => {
            panic!("Received no claim limit value.");
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn member_screening_reinvite_url_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let mut account_age_option: CreateInputText = CreateInputText::new(InputTextStyle::Short, "Invite link", "SETTINGS::MEMBER_SCREENING::REINVITE_URL::SET")
        .min_length(1)
        .max_length(40)
        .placeholder("e.g. https://discord.gg/xCNnXYp9eR")
        .required(true);

    if &guild.reinvite_url != "" {
        account_age_option = account_age_option.value(&guild.reinvite_url);
    }

    let modal_components: Vec<CreateActionRow> = vec![CreateActionRow::InputText(account_age_option)];

    let modal: CreateModal = CreateModal::new("SETTINGS::MEMBER_SCREENING::REINVITE_URL::SET", "Set re-invite URL")
        .components(modal_components);

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Modal(modal)
    ).await.expect("Couldn't send!");
}

pub(super) async fn member_screening_reinvite_url_set(ctx: SerenityContext, interaction: ModalInteraction, values: &mut HashMap<String, String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get("SETTINGS::MEMBER_SCREENING::REINVITE_URL::SET") {
        Some(url) => {
            if !url.contains("discord.gg") && !url.contains("discord.com/invite") {
                message = CreateInteractionResponseMessage::new()
                    .content("Please enter a valid discord.gg URL.")
                    .ephemeral(true);
            }
            else {
                guild.reinvite_url = url.to_string();
                guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
                message = CreateInteractionResponseMessage::new()
                    .content(format!("Re-invite URL set to {url}"))
                    .ephemeral(true);
            }
        }
        None => {
            guild.reinvite_url = "".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Re-invite URL removed. Users who are kicked for having an account too young will not be invited back to the server.")
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

// i hate this function name why is it so long why are there so many underscores how can i make it better
pub(super) async fn member_screening_member_age_check_log_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_channel_menu(&guild.important_channel_ids.claim_list);

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::MEMBER_SCREENING::MEMBER_AGE_CHECK::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a channel");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Member screening  >  Set member age log channel\n\nSet the channel where users' answers to their age and DOB will be broadcast.{GENERIC_CHANNEL_MENU_ADVICE} **To disable this feature, use the toggle features menu instead.**"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening#set-broadcast-channel".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn member_screening_member_age_check_log_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_channel_ids.member_age_broadcast = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Member age log channel set to <#{id}>."))
                .ephemeral(true);
        }
        None => {
            guild.important_channel_ids.member_age_broadcast = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Member age log channel removed. Member ages will not be broadcast anywhere.")
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}
