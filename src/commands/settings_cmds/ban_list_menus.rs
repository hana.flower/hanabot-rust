use poise::serenity_prelude::{ComponentInteraction, CreateActionRow, CreateInteractionResponse, CreateInteractionResponseMessage, CreateSelectMenu, CreateSelectMenuKind};
use poise::serenity_prelude::client::Context as SerenityContext;

use crate::commands::settings_cmds::{gen_footer_row, gen_single_channel_menu, GENERIC_CHANNEL_MENU_ADVICE};
use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::GuildEntry;
use crate::utils::couch_db::CouchDB;

pub(super) async fn ban_list_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_channel_menu(&guild.important_channel_ids.ban_list);

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::BAN_LIST::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a channel");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Ban list\n\nSet the channel to broadcast bans to, or no channel if you do not want bans to be broadcast anywhere.{GENERIC_CHANNEL_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Moderation".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn ban_list_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_channel_ids.ban_list = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Ban list channel set to <#{id}>."))
                .ephemeral(true);
        }
        None => {
            guild.important_channel_ids.ban_list = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Ban list channel removed. Bans will not be broadcast anywhere.")
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}
