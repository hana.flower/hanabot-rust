use poise::serenity_prelude::{ComponentInteraction, CreateActionRow, CreateInteractionResponse, CreateInteractionResponseFollowup, CreateInteractionResponseMessage, CreateSelectMenu, CreateSelectMenuKind, CreateSelectMenuOption};
use poise::serenity_prelude::client::Context as SerenityContext;
use poise::serenity_prelude::ReactionType::Unicode;

use crate::commands::settings_cmds::gen_footer_row;
use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::{FeaturesFlags, GuildEntry};
use crate::utils::couch_db::CouchDB;

async fn gen_toggle_features_menu(guild: GuildEntry) -> CreateActionRow {
    // Initialise all to disabled for now.
    let mut select_menu_options: Vec<CreateSelectMenuOption> = vec![
        CreateSelectMenuOption::new("Ban list (disabled)", "SETTINGS::TOGGLE_FEATURES::BAN_LIST::ON").description("Disabled").emoji(Unicode("🔨".to_string())),
        CreateSelectMenuOption::new("Welcoming (disabled)", "SETTINGS::TOGGLE_FEATURES::WELCOMING::ON").description("Disabled").emoji(Unicode("👋".to_string())),
        CreateSelectMenuOption::new("Claiming (disabled)", "SETTINGS::TOGGLE_FEATURES::CLAIMING::ON").description("Disabled").emoji(Unicode("🙇".to_string())),
        CreateSelectMenuOption::new("Member age check (disabled)", "SETTINGS::TOGGLE_FEATURES::MEMBER_AGE_CHECK::ON").description("Disabled").emoji(Unicode("🎂".to_string())),
        CreateSelectMenuOption::new("Cross-verification (disabled)", "SETTINGS::TOGGLE_FEATURES::CROSS_VERIFY::ON").description("Disabled").emoji(Unicode("🪪".to_string()))
    ];

    let enabled_features: Vec<FeaturesFlags> = guild.get_enabled_features();
    for feature in enabled_features {
        match feature {
            FeaturesFlags::BanList => {
                select_menu_options[0] = CreateSelectMenuOption::new("Ban list (enabled)", "SETTINGS::TOGGLE_FEATURES::BAN_LIST::OFF").description("Enabled").emoji(Unicode("🔨".to_string()))
            }
            FeaturesFlags::Welcoming => {
                select_menu_options[1] = CreateSelectMenuOption::new("Welcoming (enabled)", "SETTINGS::TOGGLE_FEATURES::WELCOMING::OFF").description("Enabled").emoji(Unicode("👋".to_string()))
            }
            FeaturesFlags::Claiming => {
                select_menu_options[2] = CreateSelectMenuOption::new("Claiming (enabled)", "SETTINGS::TOGGLE_FEATURES::CLAIMING::OFF").description("Enabled").emoji(Unicode("🙇".to_string()))
            }
            FeaturesFlags::MemberAgeCheck => {
                select_menu_options[3] = CreateSelectMenuOption::new("Member age check (enabled)", "SETTINGS::TOGGLE_FEATURES::MEMBER_AGE_CHECK::OFF").description("Enabled").emoji(Unicode("🎂".to_string()))
            }
            FeaturesFlags::CrossVerify => {
                select_menu_options[4] = CreateSelectMenuOption::new("Cross-verification (enabled)", "SETTINGS::TOGGLE_FEATURES::CROSS_VERIFY::OFF").description("Enabled").emoji(Unicode("🪪".to_string()))
            }
            // A few features are not supported by toggle features.
            _ => continue
        }
    }

    let select_menu: CreateSelectMenuKind = CreateSelectMenuKind::String {
        options: select_menu_options
    };

    let features_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::TOGGLE_FEATURES::SET".to_string(), select_menu)
        .min_values(1)
        .placeholder("Select features to toggle.");

    CreateActionRow::SelectMenu(features_select_menu)
}

pub(super) async fn toggle_features_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let row1: CreateActionRow = gen_toggle_features_menu(guild).await;

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content("## Hanabot settings  >  Toggle features\n\nTurn the bot's features on or off. If a feature is not listed here, check its individual settings instead.")
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Toggle-Features".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(super) async fn toggle_feature(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let mut confirmation_message: CreateInteractionResponseFollowup = CreateInteractionResponseFollowup::new()
        .ephemeral(true);

    let args: Vec<&str> = values[0].split("::").collect::<Vec<&str>>();
    let feature: &str = args[2];

    if args[3] == "ON" {
        match feature {
            "BAN_LIST" => {
                guild.features += FeaturesFlags::BanList as usize;
                confirmation_message = confirmation_message.content("Enabled ban list.");
            },
            "WELCOMING" => {
                guild.features += FeaturesFlags::Welcoming as usize;
                confirmation_message = confirmation_message.content("Enabled welcoming.");
            },
            "CLAIMING" => {
                guild.features += FeaturesFlags::Claiming as usize;
                confirmation_message = confirmation_message.content("Enabled claiming.");
            },
            "MEMBER_AGE_CHECK" => {
                guild.features += FeaturesFlags::MemberAgeCheck as usize;
                confirmation_message = confirmation_message.content("Enabled member age check.");
            },
            "CROSS_VERIFY" => {
                guild.features += FeaturesFlags::CrossVerify as usize;
                confirmation_message = confirmation_message.content("Enabled cross-verification.");
            },
            _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
        }
    }
    else {
        match feature {
            "BAN_LIST" => {
                guild.features -= FeaturesFlags::BanList as usize;
                confirmation_message = confirmation_message.content("Disabled ban list.");
            },
            "WELCOMING" => {
                guild.features -= FeaturesFlags::Welcoming as usize;
                confirmation_message = confirmation_message.content("Disabled welcoming.");
            },
            "CLAIMING" => {
                guild.features -= FeaturesFlags::Claiming as usize;
                confirmation_message = confirmation_message.content("Disabled claiming.");
            },
            "MEMBER_AGE_CHECK" => {
                guild.features -= FeaturesFlags::MemberAgeCheck as usize;
                confirmation_message = confirmation_message.content("Disabled member age check.");
            },
            "CROSS_VERIFY" => {
                guild.features -= FeaturesFlags::CrossVerify as usize;
                confirmation_message = confirmation_message.content("Disabled cross-verification.");
            },
            _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
        }
    }
    guild.push_guild(&couch_db).await.expect("Couldn't update DB.");

    let row1: CreateActionRow = gen_toggle_features_menu(guild).await;

    let toggle_features_message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content("## Hanabot settings  >  Toggle features\n\nTurn the bot's features on or off. If a feature is not listed here, check its individual settings instead.")
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Toggle-Features".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        &ctx.http,
        CreateInteractionResponse::UpdateMessage(toggle_features_message)
    ).await.expect("Couldn't send!");

    interaction.create_followup(
        &ctx.http,
        confirmation_message
    ).await.expect("Couldn't send!");
}
