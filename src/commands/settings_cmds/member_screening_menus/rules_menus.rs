use std::collections::HashMap;
use std::str::FromStr;

use poise::serenity_prelude::{ButtonStyle, Channel, ChannelId, ChannelType, ComponentInteraction, CreateActionRow, CreateAllowedMentions, CreateButton, CreateInputText, CreateInteractionResponse, CreateInteractionResponseMessage, CreateMessage, CreateModal, CreateSelectMenu, CreateSelectMenuKind, DiscordJsonError, Error, HttpError, InputTextStyle, Message, ModalInteraction, Permissions};
use poise::serenity_prelude::client::Context as SerenityContext;
use poise::serenity_prelude::ReactionType::Unicode;
use poise::serenity_prelude::ChannelType::{News, NewsThread, PrivateThread, PublicThread, Text};

use crate::commands::settings_cmds::{gen_footer_row, gen_single_role_menu, gen_single_channel_menu, GENERIC_CHANNEL_MENU_ADVICE, GENERIC_ROLE_MENU_ADVICE};
use crate::CtxCouchDB;
use crate::guild_settings::guilds_db_wrapper::{GuildEntry, MessageLocation};
use crate::utils::couch_db::CouchDB;

pub(in super::super::super::settings_cmds) async fn rules_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let member_role_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::RULES::MEMBER_ROLE".to_string())
        .label("Set member role")
        .style(ButtonStyle::Primary);
    let create_message_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::RULES::CREATE_MESSAGE".to_string())
        .label("Create rules verification message")
        .style(ButtonStyle::Primary);
    // Swap agree and disagree may or may not be implemented.
    let acceptance_message_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::RULES::WELCOME_IN".to_string())
        .label("Change rules acceptance message")
        .style(ButtonStyle::Primary);
    let preview_acceptance_message_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::RULES::WELCOME_IN::PREVIEW".to_string())
        .label("Preview rules acceptance message")
        .style(ButtonStyle::Primary);

    let row1: CreateActionRow = CreateActionRow::Buttons(vec![
        member_role_button,
        create_message_button,
        acceptance_message_button,
        preview_acceptance_message_button
    ]);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content("## Hanabot settings  >  Member screening  >  Rules\n\nChange rules confirmation settings.")
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Rules".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(in super::super::super::settings_cmds) async fn rules_member_role_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let select_menu: CreateSelectMenuKind = gen_single_role_menu(&guild.important_role_ids.member);

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::MEMBER_SCREENING::RULES::MEMBER_ROLE::SET".to_string(), select_menu)
        .min_values(0)
        .placeholder("Select a role");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Member screening  >  Rules  >  Set member role\n\nSet the role given to members when they agree to the rules.{GENERIC_ROLE_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Rules".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(in super::super::super::settings_cmds) async fn rules_member_role_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;

    match values.get(0) {
        Some(id) => {
            guild.important_role_ids.member = id.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content(format!("Member role set to <@&{id}>."))
                .ephemeral(true)
                .allowed_mentions(CreateAllowedMentions::new()); // Disable all mentions.
        }
        None => {
            guild.important_role_ids.member = "0".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Member role removed. Members will not get any role for agreeing to the rules.")
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(in super::super::super::settings_cmds) async fn rules_create_message_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    // The "0" is a placeholder for the function.
    let select_menu: CreateSelectMenuKind = gen_single_channel_menu(&"0".to_string());

    let claim_list_select_menu: CreateSelectMenu = CreateSelectMenu::new("SETTINGS::MEMBER_SCREENING::RULES::CREATE_MESSAGE::SET".to_string(), select_menu)
        .min_values(1)
        .placeholder("Select a channel");

    let row1: CreateActionRow = CreateActionRow::SelectMenu(claim_list_select_menu);

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(format!("## Hanabot settings  >  Member screening  >  Rules  >  Create rules verification message\n\nSelect a channel to send the rules verification message in.{GENERIC_CHANNEL_MENU_ADVICE}"))
        .components(vec![row1, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening".to_string(), Some(&interaction.data.custom_id))]);
    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::UpdateMessage(message)
    ).await.expect("Couldn't send!");
}

pub(in super::super::super::settings_cmds) async fn rules_create_message_set(ctx: SerenityContext, interaction: ComponentInteraction, values: &Vec<String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();
    let new_channel_id: ChannelId;

    match values.get(0) {
        Some(id) => {
            new_channel_id = ChannelId::from_str(id).unwrap();
        }
        None => {
            let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
                .content("No channel was found. Not sure what happened there, but try again.")
                .ephemeral(true);
            interaction.create_response(
                ctx.http,
                CreateInteractionResponse::Message(message)).await.expect("Couldn't send!");
            panic!("Somehow no channel was received for the rules verification message.")
        }
    }

    let new_channel = if let Channel::Guild(channel) = new_channel_id.to_channel(&ctx.http).await.unwrap() {
        channel
    } else {
        panic!("This case should never happen, but I need to include it to make the compiler happy.");
    };

    // Check for send messages permission
    let current_user_id = &ctx.cache.current_user().clone().id;
    let permissions: Permissions = new_channel.permissions_for_user(&ctx, current_user_id).expect("Couldn't calculate permissions!");

    if (new_channel.kind == Text) || (new_channel.kind == News) {
        if !permissions.send_messages() {
            let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
                .content("I do not have permission to send messages in that channel!")
                .ephemeral(true);
            interaction.create_response(
                &ctx.http,
                CreateInteractionResponse::Message(message)
            ).await.expect("Couldn't send!");
            return;
        }
    }
    else if (new_channel.kind == PublicThread) || (new_channel.kind == PrivateThread) || (new_channel.kind == NewsThread) {
        // currently doesn't work fully (but who's making their rules in a thread anyway lol)
        // I believe this is an issue with serenity calculating the wrong permissions
        if !permissions.send_messages_in_threads() {
            let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
                .content("I do not have permission to send messages in that thread!")
                .ephemeral(true);
            interaction.create_response(
                &ctx.http,
                CreateInteractionResponse::Message(message)
            ).await.expect("Couldn't send!");
            return;
        }
    }

    let mut message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content("Done!")
        .ephemeral(true);

    // Delete the old message, if one was ever sent.
    let rules_message_location: MessageLocation;

    // todo: make it a permission check (am lazy rn)
    if guild.rules_message_location.len() == 2 {
        let could_delete_old_message: bool;
        rules_message_location = MessageLocation::new(guild.rules_message_location).unwrap();

        match &ctx.http.delete_message(rules_message_location.channel_id, rules_message_location.message_id, Some("Creating a new rules verification message")).await {
            Ok(_) => { could_delete_old_message = true; }
            Err(error) => {
                match error {
                    Error::Http(error) => {
                        match error {
                            HttpError::UnsuccessfulRequest(error_response) => {
                                let discord_error: &DiscordJsonError = &error_response.error;
                                if discord_error.code == 50001 {
                                    could_delete_old_message = false;
                                }
                                else {
                                    could_delete_old_message = true;
                                }
                            }
                            _ => { could_delete_old_message = false; }
                        }
                    }
                    _ => { could_delete_old_message = false; }
                }
            }
        }

        if could_delete_old_message == false {
            // I wish there was an easier way to do this
            let guild_id = interaction.guild_id.unwrap();
            let channel_id = rules_message_location.channel_id;
            let message_id = rules_message_location.message_id;

            message = message.content(format!("Done! However, the old message at https://discord.com/channels/{guild_id}/{channel_id}/{message_id} could not be deleted."));
        }
    }

    let rules_message_rows: Vec<CreateActionRow> = vec![
        CreateActionRow::Buttons(vec![
            CreateButton::new("RULES::ACCEPT")
                .emoji(Unicode("✅".parse().unwrap()))
                .style(ButtonStyle::Success)
        ])
    ];

    let rules_message: CreateMessage = CreateMessage::new()
        .content("Thank you for reading the rules! Click on the ✅ button below to enter.")
        .components(rules_message_rows);
    let created_message: Message = new_channel_id.send_message(&ctx.http, rules_message).await.expect("Couldn't send rules message!");

    guild.rules_message_location = vec![created_message.channel_id.to_string(), created_message.id.to_string()];
    guild.push_guild(&couch_db).await.expect("Couldn't update DB.");

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(in super::super::super::settings_cmds) async fn rules_welcome_in_message_menu(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let mut message_option: CreateInputText = CreateInputText::new(InputTextStyle::Paragraph, "Rules acceptance message", "CONTENT")
        .min_length(1)
        .max_length(2000)
        .placeholder("e.g. Welcome in! Please check <#1234567890> for instructions on verifying your age.")
        .required(true);

    // It should always be an actual value, but this is just in case something goes wrong.
    if &guild.messages.rules_accepted != "" {
        message_option = message_option.value(&guild.messages.rules_accepted);
    }

    let modal_components: Vec<CreateActionRow> = vec![CreateActionRow::InputText(message_option)];

    let modal: CreateModal = CreateModal::new("SETTINGS::MEMBER_SCREENING::RULES::WELCOME_IN::SET", "Set rules acceptance message")
        .components(modal_components);

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Modal(modal)
    ).await.expect("Couldn't send!");
}

pub(in super::super::super::settings_cmds) async fn rules_welcome_in_message_set(ctx: SerenityContext, interaction: ModalInteraction, values: &mut HashMap<String, String>) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let mut guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage;
    let preview_acceptance_message_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING::RULES::WELCOME_IN::PREVIEW".to_string())
        .label("Preview")
        .style(ButtonStyle::Primary);

    match values.get("CONTENT") {
        Some(content) => {
            guild.messages.rules_accepted = content.to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Rules acceptance message updated.")
                .components(vec![CreateActionRow::Buttons(vec![preview_acceptance_message_button])])
                .ephemeral(true);
        }
        // This shouldn't happen but I already implemented it so it's here now.
        None => {
            guild.messages.rules_accepted = "".to_string();
            guild.push_guild(&couch_db).await.expect("Couldn't update DB.");
            message = CreateInteractionResponseMessage::new()
                .content("Rules acceptance message removed. It will default to \"Welcome in!\"")
                .components(vec![CreateActionRow::Buttons(vec![preview_acceptance_message_button])])
                .ephemeral(true);
        }
    }

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}

pub(in super::super::super::settings_cmds) async fn rules_welcome_in_message_preview(ctx: SerenityContext, interaction: ComponentInteraction) {
    let couch_db: CouchDB = ctx.data.read().await.get::<CtxCouchDB>().unwrap().clone();
    let guild: GuildEntry = GuildEntry::get_guild(&couch_db, &interaction.guild_id.unwrap()).await.unwrap();

    let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
        .content(guild.messages.rules_accepted)
        .ephemeral(true);

    interaction.create_response(
        ctx.http,
        CreateInteractionResponse::Message(message)
    ).await.expect("Couldn't send!");
}
