use claim::claim;
use edit::edit;
use super::super::Data;

pub mod claim_delete_queue;
pub mod claim_db_wrapper;
pub mod claim;
pub mod edit;

type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>; // This is the context type for the commands! Data is user defined in main.rs!

/// The `claim_command_group`!
#[poise::command(slash_command, subcommands("claim", "edit"))]
#[allow(clippy::unused_async)]
pub async fn claiming(_ctx: Context<'_>) -> Result<(), Error> {
    Ok(())
} // This is a parent command!
