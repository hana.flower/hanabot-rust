use std::collections::HashMap;
use std::string::ToString;

use poise::CreateReply;
use poise::serenity_prelude::{ButtonStyle, ChannelId, ComponentInteraction, CreateActionRow, CreateButton, CreateInteractionResponse, CreateInteractionResponseMessage, CreateSelectMenuKind, ModalInteraction, RoleId};
use poise::serenity_prelude::ChannelType::{News, PrivateThread, PublicThread, Text, NewsThread};
use poise::serenity_prelude::client::Context as SerenityContext;
use poise::serenity_prelude::ComponentInteractionDataKind;
use poise::serenity_prelude::ReactionType::Unicode;

use crate::Data;

mod claiming_menus;
mod ban_list_menus;
mod toggle_features_menus;
mod cross_verify_menus;
mod member_screening_menus;

type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data , Error>;

// These are general notes that apply to all select menus of certain types and are meant to be formatted into messages.
const GENERIC_ROLE_MENU_ADVICE: &str = "\n\nYou may need to type the role's name if it doesn't appear in the dropdown.";
const GENERIC_CHANNEL_MENU_ADVICE: &str = "\n\nYou may need to type the channel's name if it doesn't appear in the dropdown, but note that only text and announcement channels, as well as threads, are valid.";
const GENERIC_ROLE_SELECTION_ADVICE: &str = " Please note that you must manually migrate the roles of users to the new role for now.";
const GENERIC_ROLE_SELECTION_REMOVAL_ADVICE: &str = " Please note that you must manually remove the roles of users for now.";

/// Helper function for other menus to quickly generate a role select menu with one default value.
fn gen_single_role_menu(role_id: &String) -> CreateSelectMenuKind {
    if role_id == "0" {
        CreateSelectMenuKind::Role {
            default_roles: None
        }
    }
    else {
        CreateSelectMenuKind::Role {
            default_roles: Some(vec![RoleId::new(role_id.parse().unwrap())]),
        }
    }
}

/// Helper function for other menus to quickly generate a channel select menu with one default value.
fn gen_single_channel_menu(channel_id: &String) -> CreateSelectMenuKind {
    if channel_id == "0" {
        CreateSelectMenuKind::Channel {
            channel_types: Some(vec![Text, News, PublicThread, PrivateThread, NewsThread]),
            default_channels: None,
        }
    }
    else {
        CreateSelectMenuKind::Channel {
            channel_types: Some(vec![Text, News, PublicThread, PrivateThread, NewsThread]),
            default_channels: Some(vec![ChannelId::new(channel_id.parse().unwrap())]),
        }
    }
}

/// Generates the footer action row for all settings messages, including the help button, bot
/// version, and back button (only generated if a custom_id is passed).
fn gen_footer_row(help_url: String, custom_id: Option<&String>) -> CreateActionRow {
    let version_button: CreateButton = CreateButton::new_link("https://gitlab.com/hana.flower/hanabot")
        .label("Bot version (commit hash)");
    let help_button: CreateButton = CreateButton::new_link(help_url)
        .label("Help!");
    match custom_id {
        Some(custom_id) => {
            let mut args: Vec<&str> = custom_id.split("::").collect::<Vec<&str>>();
            let mut back_custom_id = String::new();
            args.pop();

            for item in &args {
                back_custom_id.push_str(item);
                back_custom_id.push_str("::");
            }

            // To remove the trailing ::
            back_custom_id.pop();
            back_custom_id.pop();

            let back_button: CreateButton = CreateButton::new(back_custom_id)
                .label("Back")
                .emoji(Unicode("⬅️".to_string()))
                .style(ButtonStyle::Danger);
            CreateActionRow::Buttons(vec![
                version_button,
                help_button,
                back_button])
        }
        None => {
            CreateActionRow::Buttons(vec![
                version_button,
                help_button])
        }
    }
}

/// I couldn't find a way to make the settings() function also edit messages, so this will do for now.
fn gen_base_menu_rows() -> Vec<CreateActionRow> {
    let claiming_button: CreateButton = CreateButton::new("SETTINGS::CLAIMING".to_string())
        .label("Claiming")
        .style(ButtonStyle::Primary);
    let ban_list_button: CreateButton = CreateButton::new("SETTINGS::BAN_LIST".to_string())
        .label("Ban list")
        .style(ButtonStyle::Primary);
    let member_screening_button: CreateButton = CreateButton::new("SETTINGS::MEMBER_SCREENING".to_string())
        .label("Member screening")
        .style(ButtonStyle::Primary);
    // Leaving welcoming unfinished for now while I figure out what to do with it
    let welcoming_button: CreateButton = CreateButton::new("SETTINGS::WELCOMING".to_string())
        .label("Welcoming")
        .style(ButtonStyle::Primary);
    let toggle_features_button: CreateButton = CreateButton::new("SETTINGS::TOGGLE_FEATURES".to_string())
        .label("Toggle features")
        .style(ButtonStyle::Primary);
    let cross_verification_button: CreateButton = CreateButton::new("SETTINGS::CROSS_VERIFY".to_string())
        .label("Cross-verification")
        .style(ButtonStyle::Primary);

    let row1: CreateActionRow = CreateActionRow::Buttons(vec![
        claiming_button,
        ban_list_button,
        member_screening_button,
        welcoming_button,
        toggle_features_button]);

    let row2: CreateActionRow = CreateActionRow::Buttons(vec![
        cross_verification_button]);

    vec![row1, row2, gen_footer_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Commands/Settings".to_string(), None)]
}

#[poise::command(slash_command, default_member_permissions = "MANAGE_GUILD", guild_only = true)]
/// Change my settings!
///
/// ...weird that the description is the first doc line, but whatever, it works I guess
///
/// Anyway, this command brings up a menu that allows server staff to modify bot settings, such
/// as the claimed role, the enabled features, where to broadcast claims, etc.
/// The submenus are navigated by button interactions, so this base command only brings up
/// the first menu.
pub async fn settings(ctx: Context<'_>) -> Result<(), Error> {
    let message: CreateReply = CreateReply {
        content: Some("## Hanabot settings\n\nModify the bot's settings for this server.".to_string()),
        embeds: vec![],
        attachments: vec![],
        ephemeral: Some(true),
        components: Some(gen_base_menu_rows()),
        allowed_mentions: None,
        reply: false,
        __non_exhaustive: (),
    };
    ctx.send(message).await.expect("Couldn't Ping!");
    Ok(())
}

/// Determines which submenu of the settings menu to go to, or what setting to change.
pub async fn settings_menu(ctx: SerenityContext, interaction: ComponentInteraction, args: Vec<&str>) {
    // All the options, if it's a select menu, as Strings.
    let option_values: &mut Vec<String> = &mut Vec::new();
    match &interaction.data.kind {
        ComponentInteractionDataKind::Button => {}
        ComponentInteractionDataKind::StringSelect { values } => { *option_values = values.clone(); }
        ComponentInteractionDataKind::UserSelect { values } => {
            for value in values {
                option_values.push(value.to_string());
            }
        }
        ComponentInteractionDataKind::RoleSelect { values } => {
            for value in values {
                option_values.push(value.to_string());
            }
        }
        ComponentInteractionDataKind::MentionableSelect { values } => {
            for value in values {
                option_values.push(value.to_string());
            }
        }
        ComponentInteractionDataKind::ChannelSelect { values } => {
            for value in values {
                option_values.push(value.to_string());
            }
        }
        ComponentInteractionDataKind::Unknown(_) => {}
    }

    match args.len() {
        // Base settings menu when accessed via the back button.
        1 => {
            let message: CreateInteractionResponseMessage = CreateInteractionResponseMessage::new()
                .content("## Hanabot settings\n\nModify the bot's settings for this server.")
                .components(gen_base_menu_rows());
            interaction.create_response(
                ctx.http,
                CreateInteractionResponse::UpdateMessage(message))
                .await.expect("Couldn't send!");
        },
        // Everything else
        2 => {
            match args[1] {
                "CLAIMING" => { claiming_menus::claiming_menu(ctx, interaction).await },
                "BAN_LIST" => { ban_list_menus::ban_list_menu(ctx, interaction).await },
                "MEMBER_SCREENING" => { member_screening_menus::member_screening_menu(ctx, interaction).await },
                "WELCOMING" => {},
                "TOGGLE_FEATURES" => { toggle_features_menus::toggle_features_menu(ctx, interaction).await },
                "CROSS_VERIFY" => { cross_verify_menus::cross_verify_menu(ctx, interaction).await },
                _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
            }
        },
        3 => {
            match args[2] {
                "CLAIMED_ROLE" => { claiming_menus::claiming_claimed_role_menu(ctx, interaction).await },
                "MASTER_ROLE" => { claiming_menus::claiming_master_role_menu(ctx, interaction).await },
                "CLAIM_LIST" => { claiming_menus::claiming_claim_list_menu(ctx, interaction).await },
                "CLAIMING_CHANNEL" => { claiming_menus::claiming_claiming_menu(ctx, interaction).await },
                "CLAIM_LIMIT" => { claiming_menus::claiming_claim_limit_menu(ctx, interaction).await },

                "MAJOR_ROLE" => { cross_verify_menus::cross_verify_major_role_menu(ctx, interaction).await },
                "LOG_CHANNEL" => { cross_verify_menus::cross_verify_log_channel_menu(ctx, interaction).await },

                "ACCOUNT_AGE" => { member_screening_menus::member_screening_account_age_menu(ctx, interaction).await },
                "REINVITE_URL" => { member_screening_menus::member_screening_reinvite_url_menu(ctx, interaction).await }
                "RULES" => { member_screening_menus::rules_menus::rules_menu(ctx, interaction).await },
                "MEMBER_AGE_CHECK" => { member_screening_menus::member_screening_member_age_check_log_menu(ctx, interaction).await }

                "SET" => match args[1] {
                    "BAN_LIST" => { ban_list_menus::ban_list_set(ctx, interaction, option_values).await },
                    "TOGGLE_FEATURES" => { toggle_features_menus::toggle_feature(ctx, interaction, option_values).await },
                    _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
                },

                _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
            }
        },
        4 => {
            match args[3] {
                "MEMBER_ROLE" => { member_screening_menus::rules_menus::rules_member_role_menu(ctx, interaction).await },
                "CREATE_MESSAGE" => { member_screening_menus::rules_menus::rules_create_message_menu(ctx, interaction).await },
                "WELCOME_IN" => { member_screening_menus::rules_menus::rules_welcome_in_message_menu(ctx, interaction).await }

                "SET" => match args[2] {
                    "CLAIMED_ROLE" => { claiming_menus::claiming_claimed_role_set(ctx, interaction, option_values).await },
                    "MASTER_ROLE" => { claiming_menus::claiming_master_role_set(ctx, interaction, option_values).await },
                    "CLAIM_LIST" => { claiming_menus::claiming_claim_list_set(ctx, interaction, option_values).await },
                    "CLAIMING_CHANNEL" => { claiming_menus::claiming_claiming_set(ctx, interaction, option_values).await },

                    "MEMBER_AGE_CHECK" => { member_screening_menus::member_screening_member_age_check_log_set(ctx, interaction, option_values).await },

                    "MAJOR_ROLE" => { cross_verify_menus::cross_verify_major_role_set(ctx, interaction, option_values).await },
                    "LOG_CHANNEL" => { cross_verify_menus::cross_verify_log_channel_set(ctx, interaction, option_values).await },

                    _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
                },
                _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
            }
        },
        5 => {
            match args[4] {
                "SET" => match args[3] {
                    "CLAIMED_ROLE" => { claiming_menus::claiming_claimed_role_set(ctx, interaction, option_values).await },
                    "MASTER_ROLE" => { claiming_menus::claiming_master_role_set(ctx, interaction, option_values).await },
                    "CLAIM_LIST" => { claiming_menus::claiming_claim_list_set(ctx, interaction, option_values).await },
                    "CLAIMING" => { claiming_menus::claiming_claiming_set(ctx, interaction, option_values).await },

                    "MEMBER_ROLE" => { member_screening_menus::rules_menus::rules_member_role_set(ctx, interaction, option_values).await }
                    "CREATE_MESSAGE" => { member_screening_menus::rules_menus::rules_create_message_set(ctx, interaction, option_values).await }

                    "MAJOR_ROLE" => { cross_verify_menus::cross_verify_major_role_set(ctx, interaction, option_values).await },
                    "LOG_CHANNEL" => { cross_verify_menus::cross_verify_log_channel_set(ctx, interaction, option_values).await },

                    _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
                },
                "PREVIEW" => match args[3] {
                    "WELCOME_IN" => { member_screening_menus::rules_menus::rules_welcome_in_message_preview(ctx, interaction).await }

                    _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
                }
                _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
            }
        },
        _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
    }
}

pub async fn settings_menu_modal(ctx: SerenityContext, interaction: ModalInteraction, args: Vec<&str>, option_values: &mut HashMap<String, String>) {
    match args.len() {
        4 => {
            match args[3] {
                "SET" => match args[2] {
                    "CLAIM_LIMIT" => { claiming_menus::claiming_claim_limit_set(ctx, interaction, option_values).await },

                    "ACCOUNT_AGE" => { member_screening_menus::member_screening_account_age_set(ctx, interaction, option_values).await },
                    "REINVITE_URL" => { member_screening_menus::member_screening_reinvite_url_set(ctx, interaction, option_values).await }

                    _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
                },
                _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
            }
        },
        5 => {
            match args[4] {
                "SET" => match args[3] {
                    "WELCOME_IN" => { member_screening_menus::rules_menus::rules_welcome_in_message_set(ctx, interaction, option_values).await }

                    _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
                },
                _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
            }
        }
        _ => { panic!("Incorrect parameters: {}", interaction.data.custom_id) }
    }
}
